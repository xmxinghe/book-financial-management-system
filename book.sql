/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 8.0.26 : Database - book
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`book` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `book`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '管理员编号',
  `admin_name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '管理员用户名',
  `password` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '管理员密码',
  `status` tinyint DEFAULT NULL COMMENT '账号状态,0：启用；1：禁用',
  `power` tinyint DEFAULT NULL COMMENT '账号权限，0：系统管理员；1:图书管理图',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `admin` */

insert  into `admin`(`id`,`admin_name`,`password`,`status`,`power`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`) values 
(1,'admin','202cb962ac59075b964b07152d234b70',0,0,'2023-11-01 10:19:05','2023-11-01 10:19:09',1,1,'系统管理员'),
(6,'xm','e10adc3949ba59abbe56e057f20f883e',0,NULL,'2023-11-07 11:57:41',NULL,NULL,NULL,NULL),
(7,'guoda','202cb962ac59075b964b07152d234b70',0,NULL,'2023-11-07 12:29:50',NULL,NULL,NULL,NULL),
(8,'lb','202cb962ac59075b964b07152d234b70',0,NULL,'2023-11-07 12:34:29',NULL,NULL,NULL,NULL),
(9,'lg','202cb962ac59075b964b07152d234b70',0,NULL,'2023-11-07 12:34:37',NULL,NULL,NULL,NULL);

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '图书编号',
  `book_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书名称',
  `type_id` int DEFAULT NULL COMMENT '图书类型编号',
  `author` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作者',
  `publishers` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '出版社',
  `inprice` decimal(10,0) DEFAULT NULL COMMENT '进价',
  `outprice` decimal(10,0) DEFAULT NULL COMMENT '售价',
  `number` int DEFAULT NULL COMMENT '图书数量',
  `publication_date` datetime DEFAULT NULL COMMENT '出版日期',
  `profile` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '简介',
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书图片',
  `status` tinyint DEFAULT NULL COMMENT '图书库存状态，0：有库存；1：无库存',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book` */

insert  into `book`(`id`,`book_name`,`type_id`,`author`,`publishers`,`inprice`,`outprice`,`number`,`publication_date`,`profile`,`remark`,`create_time`,`create_user`,`update_time`,`update_user`,`image`,`status`) values 
(1,'骆驼祥子',2,'老舍','宇宙风',36,54,20,'2023-11-01 12:35:23','著名作家，老舍的长篇小说',NULL,'2023-11-01 12:36:18',NULL,NULL,NULL,NULL,0),
(2,'三体',2,'刘慈欣','重庆出版社',26,46,50,'2023-11-01 04:39:32','著名科幻小说',NULL,NULL,NULL,NULL,NULL,'2023-11-02/202311022113120867635.jpg',0),
(3,'斗破苍穹',2,'天蚕土豆','中国致公出版社',33,58,102,'2023-11-01 04:41:17','饱受青年喜爱的长篇玄幻小说',NULL,NULL,NULL,NULL,NULL,'2023-11-02/202311022111540936318.jpg',0),
(4,'完美世界',2,'辰东','浙江人民美术出版社',32,56,50,'2023-10-31 20:42:49','一个荒古世界的传奇故事',NULL,NULL,NULL,NULL,NULL,'2023-11-02/202311022220134477742.jpg',0),
(5,'龙族',2,'江南','长江出版社',28,43,20,'2023-11-01 04:45:02','创造一个只有绘梨衣的世界',NULL,NULL,NULL,NULL,NULL,'2023-11-02/202311022221335426183.jpg',0),
(6,'假如给我三天光明',3,'海伦凯勒','中国致公出版社',16,26,60,'2023-11-01 12:49:05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),
(7,'吞噬星空',2,'我吃西红柿','无',NULL,50,46,'2023-10-30 16:00:00',NULL,NULL,'2023-11-01 11:52:53',NULL,NULL,NULL,'2023-11-02/202311022114392432533.jpg',0),
(21,'盗墓笔记',15,'南派三叔','无',NULL,58,50,'2023-11-01 04:41:17',NULL,NULL,'2023-11-02 14:24:35',NULL,NULL,NULL,'2023-11-02/202311022224027356885.jpg',0),
(22,'红楼梦',10,'曹雪芹','无',NULL,66,50,'2023-10-31 08:00:00','贾宝玉和林黛玉的缠绵爱情故事',NULL,'2023-11-03 09:35:17',NULL,NULL,NULL,'images/defaultBookImage.jpg',0),
(25,'三体',9,'刘慈欣','中国致公出版社',26,50,20,'2023-10-30 16:00:00','科幻巨作',NULL,'2023-11-05 14:55:42',NULL,NULL,NULL,'2023-11-05/202311052253401453713.jpg_temp',0),
(26,'三体',19,'刘慈欣','中国致公出版社',26,50,20,'2023-10-30 16:00:00','科幻巨作',NULL,'2023-11-05 14:55:49',NULL,NULL,NULL,'2023-11-05/202311052253401453713.jpg_temp',0),
(27,'斗罗大陆',20,'唐家三少','无',NULL,36,200,'2023-11-23 16:00:00','复活吧我的爱人！！！',NULL,'2023-11-05 15:26:22',NULL,NULL,NULL,'2023-11-05/202311052326208553711.jpg_temp',0);

/*Table structure for table `book_bill` */

DROP TABLE IF EXISTS `book_bill`;

CREATE TABLE `book_bill` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '账单编号',
  `bill_name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账单所属人',
  `status` tinyint DEFAULT NULL COMMENT '账单状态，0:支出；1：收入',
  `title` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账单标题',
  `amount` decimal(20,0) DEFAULT NULL COMMENT '账单明细金额',
  `balance` decimal(20,0) DEFAULT NULL COMMENT '余额',
  `remark` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `admin_id` int DEFAULT NULL COMMENT '管理员编号',
  `end_time` datetime DEFAULT NULL COMMENT '报表结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book_bill` */

insert  into `book_bill`(`id`,`bill_name`,`status`,`title`,`amount`,`balance`,`remark`,`create_time`,`update_time`,`create_user`,`update_user`,`admin_id`,`end_time`) values 
(1,'乐嗨',0,'买书',500,NULL,NULL,'2023-11-03 09:29:32',NULL,NULL,NULL,NULL,NULL),
(2,'国达',1,'卖书',1000,NULL,NULL,'2023-11-03 17:02:55',NULL,NULL,NULL,NULL,NULL),
(4,'admin',1,'图书报损:磨损',10,NULL,NULL,'2023-11-04 07:14:19',NULL,NULL,NULL,NULL,NULL),
(5,'admin',1,'图书报损:磨损',10,NULL,NULL,'2023-11-04 07:22:03',NULL,NULL,NULL,NULL,NULL),
(7,'admin',1,'图书报损:丢失',10,NULL,NULL,'2023-11-04 07:33:33',NULL,NULL,NULL,NULL,NULL),
(8,'admin',1,'图书报损:丢失',43,NULL,NULL,'2023-11-04 07:36:42',NULL,NULL,NULL,NULL,NULL),
(9,NULL,1,'图书外购:三体进价:22.0',440,NULL,NULL,'2023-11-05 14:55:57',NULL,NULL,NULL,NULL,NULL),
(10,'乐乐',0,'图书外购:斗罗大陆',4000,NULL,NULL,'2023-11-05 15:26:22',NULL,NULL,NULL,NULL,NULL),
(11,'xm',1,'图书购买',56,NULL,NULL,'2023-12-02 12:23:05',NULL,NULL,NULL,NULL,NULL),
(12,'chaochao',1,'图书报损',20,NULL,NULL,'2024-01-01 12:55:59',NULL,NULL,NULL,NULL,NULL),
(13,'guoda',0,'图书购买',56,NULL,NULL,'2024-01-13 13:05:46',NULL,NULL,NULL,NULL,NULL),
(16,'admin',1,'图书报损:磨损',10,NULL,NULL,'2023-11-07 06:31:21',NULL,NULL,NULL,NULL,NULL),
(17,'admin',1,'图书报损:磨损',10,NULL,NULL,'2023-11-07 06:32:56',NULL,NULL,NULL,NULL,NULL),
(18,'admin',1,'图书报损:丢失',58,NULL,NULL,'2023-11-07 06:33:34',NULL,NULL,NULL,NULL,NULL),
(19,'admin',1,'图书报损:丢失',43,NULL,NULL,'2023-11-07 06:40:25',NULL,NULL,NULL,NULL,NULL),
(20,'admin',1,'图书报损:丢失',26,NULL,NULL,'2023-11-07 06:45:55',NULL,NULL,NULL,NULL,NULL),
(21,'admin',1,'图书报损:丢失',66,NULL,NULL,'2023-11-07 07:02:04',NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `book_check` */

DROP TABLE IF EXISTS `book_check`;

CREATE TABLE `book_check` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '检查编号',
  `check_info` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '检查信息',
  `status` int DEFAULT NULL COMMENT '检查状态，0:磨损；1:丢失',
  `pay_money` decimal(20,0) DEFAULT NULL COMMENT '赔付金额',
  `checker` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '检查员姓名',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `book_id` int DEFAULT NULL COMMENT '图书编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book_check` */

insert  into `book_check`(`id`,`check_info`,`status`,`pay_money`,`checker`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`,`book_id`) values 
(1,'图书丢失',0,10,'admin',NULL,NULL,NULL,NULL,NULL,2),
(2,'图书丢失',0,10,'admin',NULL,NULL,NULL,NULL,NULL,6),
(3,'图书丢失',1,26,'admin',NULL,NULL,NULL,NULL,NULL,6),
(4,'图书丢失',1,56,'admin',NULL,NULL,NULL,NULL,NULL,4),
(5,'图书丢失',1,43,'admin',NULL,NULL,NULL,NULL,NULL,5),
(6,'图书有划痕',0,10,'admin',NULL,NULL,NULL,NULL,NULL,3),
(7,'图书有划痕',0,10,'admin',NULL,NULL,NULL,NULL,NULL,3),
(8,'图书有划痕',1,58,'admin',NULL,NULL,NULL,NULL,NULL,3),
(9,'图书丢失',1,43,'admin',NULL,NULL,NULL,NULL,NULL,5),
(10,'丢失',1,26,'admin',NULL,NULL,NULL,NULL,NULL,6),
(11,'图书丢失',1,66,'admin',NULL,NULL,NULL,NULL,NULL,22);

/*Table structure for table `book_purchase` */

DROP TABLE IF EXISTS `book_purchase`;

CREATE TABLE `book_purchase` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '图书进货编号',
  `book_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书名称',
  `supplier_id` int DEFAULT NULL COMMENT '供应商编号',
  `purchase_price` decimal(20,0) DEFAULT NULL COMMENT '图书进价',
  `innumber` int DEFAULT NULL COMMENT '进货数量',
  `purchase_person` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '进货员名称',
  `purchase_date` datetime DEFAULT NULL COMMENT '进货日期',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type_name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '类型名称',
  `product_address` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '收货地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book_purchase` */

insert  into `book_purchase`(`id`,`book_name`,`supplier_id`,`purchase_price`,`innumber`,`purchase_person`,`purchase_date`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`,`type_name`,`product_address`) values 
(1,'三体',1,28,50,'乐乐',NULL,NULL,NULL,NULL,NULL,NULL,'科幻','江西抚州'),
(2,'仙逆',1,26,100,'乐乐','2023-11-04 20:43:31',NULL,NULL,NULL,NULL,NULL,'玄幻','江西抚州'),
(3,'求魔',1,18,100,'乐乐','2023-11-04 20:52:44',NULL,NULL,NULL,NULL,NULL,'玄幻','江西抚州'),
(4,'武动乾坤',1,27,50,'国达','2023-11-04 20:56:48',NULL,NULL,NULL,NULL,NULL,'玄幻','九江共青'),
(5,'雪中悍刀行',4,26,66,'admin','2023-11-04 21:14:11',NULL,NULL,NULL,NULL,NULL,'武侠','九江共青'),
(6,'无限恐怖',4,22,80,'乐嗨','2023-11-04 21:20:43',NULL,NULL,NULL,NULL,NULL,'惊悚','九江共青'),
(7,'三体',4,22,20,'乐乐','2023-11-04 21:27:47',NULL,NULL,NULL,NULL,NULL,'科幻','江西抚州'),
(8,'斗罗大陆',1,20,200,'乐乐','2023-11-05 15:06:08',NULL,NULL,NULL,NULL,NULL,'奇幻','江西抚州'),
(9,'',NULL,NULL,NULL,'','2023-11-07 06:31:39',NULL,NULL,NULL,NULL,NULL,'',''),
(10,'死亡万花筒',4,36,60,'大乐乐','2023-11-07 07:03:22',NULL,NULL,NULL,NULL,NULL,'惊恐','江西抚州');

/*Table structure for table `book_store` */

DROP TABLE IF EXISTS `book_store`;

CREATE TABLE `book_store` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '图书仓库编号',
  `book_id` int DEFAULT NULL COMMENT '图书编号',
  `type_id` int DEFAULT NULL COMMENT '图书类型编号',
  `store_person` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '入库操作负责人',
  `status` tinyint DEFAULT NULL COMMENT '审核状态，0：未审核；1：已审核',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `supplier_id` int DEFAULT NULL COMMENT '供应商编号',
  `book_name` varchar(40) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书名称',
  `type_name` varchar(30) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书类型',
  `inprice` decimal(10,2) DEFAULT NULL COMMENT '图书进价',
  `number` int DEFAULT NULL COMMENT '外购数量',
  `outprice` decimal(10,2) DEFAULT NULL COMMENT '图书售价',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book_store` */

insert  into `book_store`(`id`,`book_id`,`type_id`,`store_person`,`status`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`,`supplier_id`,`book_name`,`type_name`,`inprice`,`number`,`outprice`) values 
(1,2,2,'admin',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(2,6,3,'admin',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
(4,2,9,'乐乐',0,NULL,NULL,NULL,NULL,NULL,NULL,'三体','科幻',28.00,50,NULL),
(5,NULL,11,'乐乐',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,NULL,'仙逆','玄幻',26.00,100,NULL),
(6,NULL,11,'乐乐',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,NULL,'求魔','玄幻',18.00,100,NULL),
(7,NULL,11,'国达',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,1,'武动乾坤','玄幻',27.00,50,NULL),
(8,NULL,NULL,'admin',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,4,'雪中悍刀行','武侠',26.00,66,NULL),
(9,NULL,NULL,'乐嗨',0,'2023-11-04 00:00:00',NULL,NULL,NULL,NULL,4,'无限恐怖','惊悚',22.00,80,NULL),
(10,2,9,'乐乐',0,'2023-11-04 21:27:47',NULL,NULL,NULL,NULL,4,'三体','科幻',22.00,20,50.00),
(11,NULL,NULL,'乐乐',1,'2023-11-05 15:06:08',NULL,NULL,NULL,NULL,1,'斗罗大陆','奇幻',20.00,200,36.00),
(12,NULL,NULL,'',0,'2023-11-07 06:31:39',NULL,NULL,NULL,NULL,NULL,'','',NULL,NULL,NULL),
(13,NULL,NULL,'大乐乐',0,'2023-11-07 07:03:22',NULL,NULL,NULL,NULL,4,'死亡万花筒','惊恐',36.00,60,NULL);

/*Table structure for table `book_type` */

DROP TABLE IF EXISTS `book_type`;

CREATE TABLE `book_type` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '图书类型编号',
  `type_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '图书类型名称',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `book_type` */

insert  into `book_type`(`id`,`type_name`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`) values 
(1,'青春','2023-11-01 12:38:09',NULL,NULL,NULL,NULL),
(2,'小说','2023-11-01 12:38:11',NULL,NULL,NULL,NULL),
(3,'文学','2023-11-01 12:38:13',NULL,NULL,NULL,NULL),
(4,'艺术','2023-11-01 12:38:14',NULL,NULL,NULL,NULL),
(5,'动漫幽默','2023-11-01 12:38:16',NULL,NULL,NULL,NULL),
(6,'娱乐时尚','2023-11-01 12:38:18',NULL,NULL,NULL,NULL),
(7,'地图地理','2023-11-01 12:38:20',NULL,NULL,NULL,NULL),
(8,'旅游','2023-11-01 12:38:22',NULL,NULL,NULL,NULL),
(9,'科幻','2023-11-01 12:38:52',NULL,NULL,NULL,NULL),
(10,'人文','2023-11-01 08:58:04',NULL,NULL,NULL,NULL),
(11,'玄幻','2023-11-01 20:29:51',NULL,NULL,NULL,NULL),
(15,'盗墓',NULL,NULL,NULL,NULL,NULL),
(17,'黑道','2023-11-03 02:47:34',NULL,NULL,NULL,NULL),
(19,'科幻','2023-11-05 14:55:47',NULL,NULL,NULL,NULL),
(20,'奇幻','2023-11-05 15:26:22',NULL,NULL,NULL,NULL);

/*Table structure for table `suppliers` */

DROP TABLE IF EXISTS `suppliers`;

CREATE TABLE `suppliers` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '供应商编号',
  `supplier_name` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '供应商名称',
  `address` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '供应商地址',
  `telephone` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '供应商电话',
  `person` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `suppliers` */

insert  into `suppliers`(`id`,`supplier_name`,`address`,`telephone`,`person`,`create_time`,`update_time`,`create_user`,`update_user`,`remark`) values 
(1,'天空书店','共青城98号','1561424577','李四',NULL,NULL,NULL,NULL,NULL),
(4,'天涯书店','江西抚州','17870010968','xm','2023-11-04 21:13:31',NULL,NULL,NULL,NULL);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户编号',
  `username` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户密码',
  `status` tinyint DEFAULT NULL COMMENT '账号状态，0：启用；1：禁用',
  `power` tinyint DEFAULT NULL COMMENT '账号权限，0：普通用户；1：会员用户',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `create_user` int DEFAULT NULL,
  `update_user` int DEFAULT NULL,
  `remark` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `telephone` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `sex` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `age` int DEFAULT NULL COMMENT '年龄',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `user` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

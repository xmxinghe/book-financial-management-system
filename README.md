# BookFinancialManagementSystem

#### 介绍
基于ssm的图书财务管理系统

#### 软件架构
软件架构说明


#### 安装教程

1.  修改jdbc.properties配置文件中url以及username，password
2.  pom.xml中下载相关依赖
3.  配置Tomcat服务器

#### 项目示例
1.首页
![输入图片说明](src/main/webapp/resources/images/image.png)

2.基础管理
 2.1：图书管理
 ![输入图片说明](src/main/webapp/resources/images/image.png)
 2.2：图书类型管理
 ![输入图片说明](src/main/webapp/resources/images/typeImage.png)

3.业务管理
 2.1:图书报损
![输入图片说明](src/main/webapp/resources/images/checkImage.png)
 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

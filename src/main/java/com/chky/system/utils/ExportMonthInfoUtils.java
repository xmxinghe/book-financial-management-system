package com.chky.system.utils;

import com.chky.book.domain.vo.BillVo;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;


/**
 * 月季表数据导出
 */ 
public class ExportMonthInfoUtils {

    /**
     * 导出出租单的数据
     *
     * @param billList
     * @param sheetName
     * @return
     */
    public static ByteArrayOutputStream exportMonthInfo(List<BillVo> billList, String sheetName) {

        //1.组装excel文档
        //1.1创建工作簿
        HSSFWorkbook workbook = new HSSFWorkbook();
        //1.2创建样式
        HSSFCellStyle baseStyle = ExportHSSFCellStyle.createBaseStyle(workbook);
        HSSFCellStyle titleStyle = ExportHSSFCellStyle.createTitleStyle(workbook);
        //1.3在工作簿创建sheet
        HSSFSheet sheet = workbook.createSheet(sheetName);
        //1.4设置sheet
        sheet.setDefaultColumnWidth(30);
        sheet.setColumnWidth(1,50*256);
        //1.5合并  例子：CellRangeAddress（2, 6000, 3, 3）；第2行起 第6000行终止 第3列开始 第3列结束。
        CellRangeAddress region1 = new CellRangeAddress(0, 0, 0, 3);
        sheet.addMergedRegion(region1);

        //1.6.1创建第一行
        int index=0;
        HSSFRow row1 = sheet.createRow(index);
        //1.6.2在第一行里面创建一个单元格
        HSSFCell row1_cell1 = row1.createCell(0);
        //1.6.3设置标题样式
        row1_cell1.setCellStyle(titleStyle);
        //1.6.4设置单元格内容
        row1_cell1.setCellValue(sheetName+"月报表信息");

        //1.7.1创建第二行
        index++;
        HSSFRow row2 = sheet.createRow(index);
        //设置行高
        row2.setHeightInPoints(150);

//        HSSFCell row2_cell1 = row2.createCell(0);
//        row2_cell1.setCellStyle(baseStyle);
//        row2_cell1.setCellValue("账单编号");

        HSSFCell row2_cell2 = row2.createCell(0);
        row2_cell2.setCellStyle(baseStyle);
        row2_cell2.setCellValue("账单所属人");

        HSSFCell row2_cell3 = row2.createCell(1);
        row2_cell3.setCellStyle(baseStyle);
        row2_cell3.setCellValue("账单金额");

        HSSFCell row2_cell4 = row2.createCell(2);
        row2_cell4.setCellStyle(baseStyle);
        row2_cell4.setCellValue("账单类型");

        HSSFCell row2_cell5 = row2.createCell(3);
        row2_cell5.setCellStyle(baseStyle);
        row2_cell5.setCellValue("账单标题");

        HSSFCell row2_cell6 = row2.createCell(4);
        row2_cell6.setCellStyle(baseStyle);
        row2_cell6.setCellValue("账单创建时间");

        HSSFCell row2_cell7 = row2.createCell(5);
        row2_cell7.setCellStyle(baseStyle);
        row2_cell7.setCellValue("账单总余额");

        int count = 0;  //计数变量
        Double num = 0D;  //账单总额
        Double input = 0D;  //账单收入
        Double output = 0D;  //账单支出
        String data = "";  //创建时间
        for (int i = 2; billList.size() > i-2; i++) {
            //打印账单信息
            HSSFRow row = sheet.createRow(i);
            HSSFCell cell = row.createCell(0);
            cell.setCellStyle(baseStyle);
            cell.setCellValue(billList.get(i-2).getBillName());
            HSSFCell cell1 = row.createCell(1);
            cell1.setCellStyle(baseStyle);
            cell1.setCellValue(billList.get(i-2).getAmount());
            HSSFCell cell2 = row.createCell(2);
            cell2.setCellStyle(baseStyle);
            Integer status = billList.get(i - 2).getStatus();
            String statusInfo = "";
            if (status == 0) {
                statusInfo = "支出";
                output+=billList.get(i - 2).getAmount();
                num -= billList.get(i - 2).getAmount();
            } else if (status==1) {
                statusInfo = "收入";
                input+=billList.get(i - 2).getAmount();
                num+=billList.get(i - 2).getAmount();
            }
            cell2.setCellValue(statusInfo);
            HSSFCell cell3 = row.createCell(3);
            cell3.setCellStyle(baseStyle);
            cell3.setCellValue(billList.get(i-2).getTitle());
            HSSFCell cell4 = row.createCell(4);
            cell4.setCellStyle(baseStyle);
//            data = String.valueOf(billList.get(i - 2).getCreateTime());
            cell4.setCellValue(billList.get(i - 2).getCreateTime().toLocaleString());
            HSSFCell cell5 = row.createCell(5);
            cell5.setCellStyle(baseStyle);
            cell5.setCellValue(num);
        }
        //最后一行
        HSSFRow lastRow = sheet.createRow(billList.size() + 2);
        HSSFCell cell = lastRow.createCell(0);
        cell.setCellStyle(baseStyle);
        cell.setCellValue("总支出:");
        HSSFCell cell1 = lastRow.createCell(1);
        cell1.setCellStyle(baseStyle);
        cell1.setCellValue(output);
        HSSFCell cell3 = lastRow.createCell(2);
        cell3.setCellStyle(baseStyle);
        cell3.setCellValue("总收入:");
        HSSFCell cell4 = lastRow.createCell(3);
        cell4.setCellStyle(baseStyle);
        cell4.setCellValue(input);
        HSSFCell cell6 = lastRow.createCell(4);
        cell6.setCellStyle(baseStyle);
        cell6.setCellValue("账单总额:");
        HSSFCell cell7 = lastRow.createCell(5);
        cell7.setCellStyle(baseStyle);
        cell7.setCellValue(num);


        //到此excel组装完毕
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //把workbook里面的数据写到outputStream
        try {
            workbook.write(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return outputStream;
    }
}

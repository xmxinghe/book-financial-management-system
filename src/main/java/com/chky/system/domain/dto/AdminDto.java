package com.chky.system.domain.dto;

import com.chky.system.domain.BaseContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdminDto extends BaseContent {
    private Integer page;
    private Integer limit;
    private String code;

    private List<Long> ids;
    private Integer id;
    private String adminName;
    private String password;
    private Integer status;
    private Integer power;
}

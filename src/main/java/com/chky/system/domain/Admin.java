package com.chky.system.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Admin extends BaseContent{
    private Integer id;
    private String adminName;
    private String password;
    private Integer status;
    private Integer power;
}

package com.chky.system.mapper;

import com.chky.system.domain.Admin;
import com.chky.system.domain.dto.AdminDto;
import com.chky.system.domain.vo.AdminVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AdminMapper {

    Admin login(AdminDto adminDto);

    List<AdminVo> list(AdminDto adminDto);

    void addAdmin(AdminDto adminDto);

    @Delete("delete from admin where id=#{id}")
    void deleteById(Integer id);

    void updateAdmin(AdminDto adminDto);

    void deleteBatch(@Param("ids") List<Long> ids);
}

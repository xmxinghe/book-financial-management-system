package com.chky.system.controller;

import com.chky.system.domain.dto.AdminDto;
import com.chky.system.service.IAdminService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {
    /*管理员管理功能模块*/
    @Resource
    private IAdminService adminService;

    /*加载所有管理员信息*/
    @RequestMapping("/loadAllAdmin")
    public DataGridView loadAllAdmin(AdminDto adminDto) {
        return adminService.loadAdmin(adminDto);
    }

    /*添加管理员信息*/
    @RequestMapping("/addAdmin")
    public ResultObj addAdmin(AdminDto adminDto) {
        adminService.addAdmin(adminDto);
        return ResultObj.ADD_SUCCESS;
    }

    /*根据编号删除管理员信息*/
    @RequestMapping("/deleteAdmin")
    public ResultObj deleteAdmin(Integer id) {
        adminService.deleteAdmin(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*根据编号修改管理员信息*/
    @RequestMapping("/updateAdmin")
    public ResultObj updateAdmin(AdminDto adminDto) {
        adminService.updateAdmin(adminDto);
        return ResultObj.UPDATE_SUCCESS;
    }

    /*批量删除管理员信息*/
    @RequestMapping("deleteBatchAdmin")
    public ResultObj deleteBatch(AdminDto adminDto) {
        if (adminDto.getIds() != null && adminDto.getIds().size() != 0) {
            adminService.deleteBatch(adminDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }
    }
}

package com.chky.system.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.chky.system.domain.Admin;
import com.chky.system.domain.dto.AdminDto;
import com.chky.system.service.IAdminService;
import com.chky.system.utils.WebUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Resource
    private IAdminService adminService;

    /*跳转至登录页面*/
    @RequestMapping("/toLogin")
    public String toLogin() {
        return "system/main/login";
    }

    /*登录方法*/
    @RequestMapping("/login")
    public String login(AdminDto adminDto, Model model) {
        //1.先判断验证码是否正确
        String code = WebUtils.getHttpSession().getAttribute("code").toString();
        if (!adminDto.getCode().equals(code)) {
            model.addAttribute("error", "验证码不正确");
            return "system/main/login";
        }

        //2.判断管理员对象是否为空
        Admin admin = adminService.login(adminDto);
        if (admin ==null) {
            model.addAttribute("error", "用户名不存在或密码错误");
            return "system/main/login";
        }else {
            if (!(admin.getStatus() == 0)) {
                model.addAttribute("error", "该账号已被禁用");
                return "system/main/login";
            }
        }
        //3.用户登录成功，跳转至index页，将admin信息存入session中
        WebUtils.getHttpSession().setAttribute("user", admin);
        return "system/main/index";
    }

    //获取验证码
    @RequestMapping("/getCode")
    public void getCode(HttpServletResponse response, HttpSession session) throws IOException {
        //定义图形验证码的⻓和宽
        LineCaptcha lineCaptcha =
                CaptchaUtil.createLineCaptcha(116, 36, 4, 5);
        session.setAttribute("code", lineCaptcha.getCode());
        ServletOutputStream outputStream =
                response.getOutputStream();
        ImageIO.write(lineCaptcha.getImage(), "JPEG", outputStream);
    }
}

package com.chky.system.service;

import com.chky.system.domain.Admin;
import com.chky.system.domain.dto.AdminDto;
import com.chky.system.utils.DataGridView;

import java.util.List;

public interface IAdminService {
    Admin login(AdminDto adminDto);

    DataGridView loadAdmin(AdminDto adminDto);

    void addAdmin(AdminDto adminDto);

    void deleteAdmin(Integer id);

    void updateAdmin(AdminDto adminDto);

    void deleteBatch(List<Long> ids);
}

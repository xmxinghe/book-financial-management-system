package com.chky.system.service.impl;

import com.chky.system.domain.Admin;
import com.chky.system.domain.dto.AdminDto;
import com.chky.system.domain.vo.AdminVo;
import com.chky.system.mapper.AdminMapper;
import com.chky.system.service.IAdminService;
import com.chky.system.utils.DataGridView;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class AdminServiceImpl implements IAdminService {
    @Resource
    private AdminMapper adminMapper;

    @Override
    public Admin login(AdminDto adminDto) {
        //根据用户名和密码查询
        //先adminVo中用户输入的密码进行md5加密操作
        String password = DigestUtils.md5DigestAsHex(adminDto.getPassword().getBytes());
//        String password = adminDto.getPassword();
        adminDto.setPassword(password);
        //调用adminMapper接口中的查询方法
        return adminMapper.login(adminDto);
    }

    @Override
    public DataGridView loadAdmin(AdminDto adminDto) {
        Page<Object> page = PageHelper.startPage(adminDto.getPage(), adminDto.getLimit());
        List<AdminVo> data=adminMapper.list(adminDto);
        return new DataGridView(page.getTotal(), data);
    }

    @Override
    public void addAdmin(AdminDto adminDto) {
        adminDto.setCreateTime(new Date());
        //查询该用户名是否存在
        List<AdminVo> adminList = adminMapper.list(adminDto);
        adminList.forEach(list->{
            boolean flag = list.getAdminName().equals(adminDto.getAdminName());
            if (flag) {
                //存在相同用户名,不允许创建
                throw new RuntimeException("该用户名已存在!");
            }
        });
        String password = DigestUtils.md5DigestAsHex(adminDto.getPassword().getBytes());
        adminDto.setPassword(password);
        adminMapper.addAdmin(adminDto);
    }

    @Override
    public void deleteAdmin(Integer id) {
        adminMapper.deleteById(id);
    }

    @Override
    public void updateAdmin(AdminDto adminDto) {
        adminDto.setCreateTime(new Date());
        String password = DigestUtils.md5DigestAsHex(adminDto.getPassword().getBytes());
        adminDto.setPassword(password);
        adminMapper.updateAdmin(adminDto);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        adminMapper.deleteBatch(ids);
    }
}

package com.chky.system.interceptor;

import com.chky.system.domain.Admin;
import com.sun.prism.impl.BaseContext;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginInterceptor implements HandlerInterceptor{

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("执行登录拦截器.....");
        Admin admin = (Admin) request.getSession().getAttribute("user");
        if (admin != null) {
            return true;  //放行
        } else {
            response.setStatus(401);
            return false;  //不放行
        }
    }
}

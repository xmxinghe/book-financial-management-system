package com.chky.book.mapper;

import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.vo.BookTypeVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookTypeMapper {
    List<BookTypeVo> list(BookTypeDto bookTypeDto);

    void add(BookTypeDto bookTypeDto);

    @Delete("delete from book_type where id=#{id}")
    void deleteById(Integer id);

    @Select("select type_name from book_type where id=#{typeId}")
    String getNameById(Integer typeId);

    void update(BookTypeDto bookTypeDto);

    void deleteBatch(@Param("ids") List<Long> ids);

    BookType getByName(String typeName);
}

package com.chky.book.mapper;

import com.chky.book.domain.dto.StoreDetailDto;
import com.chky.book.domain.dto.StoreDto;
import com.chky.book.domain.pojo.Store;
import com.chky.book.domain.vo.StoreVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StoreMapper {

    void addStoreInfo(Store store);

    List<StoreVo> list(StoreDetailDto storeDetailDto);

    @Select("select * from book_store where book_name=#{bookName}")
    Store getByName(String bookName);

    void updateStore(Store tempStore);

    void update(StoreDto storeDto);

    @Delete("delete from book_store where id=#{id}")
    void deleteById(Integer id);

    void deleteBatch(@Param("ids") List<Long> ids);
}

package com.chky.book.mapper;

import com.chky.book.domain.dto.CheckDto;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CheckMapper {
    void addCheckInfo(CheckDto checkDto);
}

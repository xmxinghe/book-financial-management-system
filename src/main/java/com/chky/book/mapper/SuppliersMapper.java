package com.chky.book.mapper;

import com.chky.book.domain.dto.SupplierDto;
import com.chky.book.domain.vo.SupplierVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SuppliersMapper {
    List<SupplierVo> list(SupplierDto supplierDto);

    void add(SupplierDto supplierDto);

    @Delete("delete from suppliers where id=#{id}")
    void deleteById(Integer id);

    void updateSupplier(SupplierDto supplierDto);

    void deleteBatch(@Param("ids") List<Long> ids);
}

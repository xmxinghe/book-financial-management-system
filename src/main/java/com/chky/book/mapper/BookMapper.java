package com.chky.book.mapper;

import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.vo.BookVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BookMapper {



    void addBook(BookDto bookDto);

    List<BookVo> list(BookDto bookDto);

    @Select("select * from book where id=#{id}")
    Book getById(Integer id);

    void updateBook(BookDto bookDto);

    @Delete("delete from book where id=#{id}")
    void deleteById(Integer id);

    void deleteBatch(@Param("ids") List<Long> ids);

    Book getByName(String bookName);
}

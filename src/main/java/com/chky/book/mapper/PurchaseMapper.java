package com.chky.book.mapper;

import com.chky.book.domain.pojo.Purchase;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PurchaseMapper {
    void addPurchaseInfo(Purchase purchase);
}

package com.chky.book.mapper;

import com.chky.book.domain.dto.BillDto;
import com.chky.book.domain.vo.BillVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface BillMapper {
    List<BillVo> list(BillDto billDto);

    void addBill(BillDto billDto);

    @Delete("delete from book_bill where id=#{id}")
    void deleteById(Integer id);

    void updateBill(BillDto billDto);

    void deleteBatch(@Param("ids") List<Long> ids);

    List<BillVo> listByTime(BillDto billDto);

    List<BillVo> listByMonth(@Param("month") String month);
}

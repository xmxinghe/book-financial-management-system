package com.chky.book.controller;

import com.chky.book.domain.dto.PurchaseDto;
import com.chky.book.service.IPurchaseService;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/purchase")
public class PurchaseController {
    @Resource
    private IPurchaseService purchaseService;

    /*图书外购业务*/
    @PostMapping("/submitPurchaseInfo")
    public ResultObj submitPurchaseInfo(PurchaseDto purchaseDto) {
//        if (
//                (purchaseDto.getBookName() == null || purchaseDto.getBookName() == "") ||
//                        (purchaseDto.getTypeName() == null || purchaseDto.getTypeName() == "") ||
//                        (purchaseDto.getPurchasePerson() == null || purchaseDto.getPurchasePerson() == "") ||
//                        (purchaseDto.getPurchasePrice() == null || purchaseDto.getPurchasePrice() == 0) ||
//                        (purchaseDto.getSupplierId() == null || purchaseDto.getSupplierId() == 0) ||
//                        (purchaseDto.getInNumber() == null || purchaseDto.getInNumber() == 0) ||
//                        (purchaseDto.getProductAddress() == null || purchaseDto.getProductAddress() == "")
//        ) {
//            return ResultObj.INFO_DELETION;
//        } else {
            purchaseService.submitPurchaseInfo(purchaseDto);
            return ResultObj.ADD_SUCCESS;
//        }
    }
}

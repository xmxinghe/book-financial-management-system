package com.chky.book.controller;

import com.chky.book.domain.dto.SupplierDto;
import com.chky.book.service.ISuppliersService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/supplier")
public class SuppliersController {

    @Resource
    private ISuppliersService suppliersService;

    /*加载所有供应商*/
    @RequestMapping("/loadAllSupplier")
    public DataGridView loadAllSupplier(SupplierDto supplierDto) {
        return suppliersService.list(supplierDto);
    }

    /*添加供应商信息*/
    @RequestMapping("/addSupplier")
    public ResultObj addSupplier(SupplierDto supplierDto) {
        suppliersService.addSupplier(supplierDto);
        return ResultObj.ADD_SUCCESS;
    }

    /*删除供应商信息*/
    @RequestMapping("/deleteSupplier")
    public ResultObj deleteSupplier(Integer id) {
        suppliersService.deleteById(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*修改供应商信息*/
    @RequestMapping("/updateSupplier")
    public ResultObj updateSupplier(SupplierDto supplierDto) {
        suppliersService.updateSupplier(supplierDto);
        return ResultObj.UPDATE_SUCCESS;
    }

    /*批量删除供应商信息*/
    @RequestMapping("/deleteBatchSupplier")
    public ResultObj deleteBatch(SupplierDto supplierDto) {
        if (supplierDto.getIds() != null && supplierDto.getIds().size() != 0) {
            suppliersService.deleteBatch(supplierDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }
    }
}

package com.chky.book.controller;

import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.domain.dto.SupplierDto;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.vo.BookTypeVo;
import com.chky.book.domain.vo.BookVo;
import com.chky.book.domain.vo.SupplierVo;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.BookTypeMapper;
import com.chky.book.mapper.SuppliersMapper;
import com.chky.system.utils.WebUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
@RequestMapping("/index")
public class IndexController {
    @Resource
    private BookTypeMapper bookTypeMapper;

    @Resource
    private BookMapper bookMapper;

    @Resource
    private SuppliersMapper suppliersMapper;
    /*跳转图书管理*/
    @RequestMapping("/toBookManager")
    public String toBookManger() {
        //先查询图书类型数据，并绑定到session域中
        List<BookTypeVo> typeList=bookTypeMapper.list(new BookTypeDto());
        WebUtils.getHttpSession().setAttribute("typeList",typeList);
        return "index/book/bookManager";
    }

    /*跳转图书类型管理*/
    @RequestMapping("/toBookTypeManager")
    public String toBookTypeManager() {
        return "index/book/bookTypeManager";
    }

    /*跳转供应商管理*/
    @RequestMapping("/toSuppliersManager")
    public String toSuppliersManager() {
        return "index/purchase/suppliersManager";
    }

    /*跳转图书流水业务*/
    @RequestMapping("/toBookBillManager")
    public String toBookBillManager() {
        return "index/bill/bookBillManager";
    }

    /*跳转图书报损业务*/
    @RequestMapping("/toBookCheckManager")
    public String toCheckCarManager() {
        //加载图书信息
        List<BookVo> bookList = bookMapper.list(new BookDto());
        WebUtils.getHttpSession().setAttribute("bookList",bookList);
        return "index/check/bookCheckManager";
    }

    /*跳转图书入库业务*/
    @RequestMapping("/toBookStoreManager")
    public String toBookStoreManager() {
        //获取供应商信息,并绑定到session域中
        List<SupplierVo> supplierList = suppliersMapper.list(new SupplierDto());
        WebUtils.getHttpSession().setAttribute("supplierList",supplierList);
        return "index/store/bookStoreManager";
    }

    /*跳转至图书外购业务*/
    @RequestMapping("/toBookPurchaseManager")
    public String toBookPurchaseManager() {
        //获取供应商信息,并绑定到session域中
        List<SupplierVo> supplierList = suppliersMapper.list(new SupplierDto());
        WebUtils.getHttpSession().setAttribute("supplierList",supplierList);
        return "index/purchase/bookPurchaseManager";
    }


    /*跳转图书报表业务*/
    @RequestMapping("/toBillExportManager")
    public String toBillExportManager() {
        return "index/bill/billExportManager";
    }

    /*跳转至管理员管理*/
    @RequestMapping("/toAdminManager")
    public String toAdminManager() {
        return "system/sys/adminManager";
    }

}

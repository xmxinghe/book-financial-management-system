package com.chky.book.controller;

import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.service.IBookTypeService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/type")
public class BookTypeController {

    @Resource
    private IBookTypeService bookTypeService;

    /*加载所有的图书类型*/
    @GetMapping("/loadAllBookType")
    public DataGridView loadAllBookType(BookTypeDto bookTypeDto) {
        return bookTypeService.loadAllBookType(bookTypeDto);
    }

    /*图书类型新增*/
    @RequestMapping("/addBookType")
    public ResultObj addBookType(BookTypeDto bookTypeDto) {
//        bookTypeDto.setCreate_time(new Date());
        bookTypeService.add(bookTypeDto);
        return ResultObj.ADD_SUCCESS;
    }

    /*图书类型删除*/
    @RequestMapping("/deleteBookType")
    public ResultObj deleteBookTypeById(Integer id) {
        bookTypeService.deleteById(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*图书类型更新*/
    @RequestMapping("/updateBookType")
    public ResultObj updateBookType(BookTypeDto bookTypeDto) {
        bookTypeService.updateBookType(bookTypeDto);
        return ResultObj.UPDATE_SUCCESS;
    }

    /*图书类型批量删除*/
    @RequestMapping("/deleteBatchBookType")
    public ResultObj deleteBatch(BookTypeDto bookTypeDto) {
        if (bookTypeDto.getIds() != null && bookTypeDto.getIds().size() != 0) {
            bookTypeService.deleteBatch(bookTypeDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }

    }
}

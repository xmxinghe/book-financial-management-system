package com.chky.book.controller;

import com.chky.book.domain.dto.CheckDto;
import com.chky.book.service.ICheckService;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/check")
public class CheckController {

    @Resource
    private ICheckService checkService;

    @PostMapping("/submitCheckInfo")
    public ResultObj submitCheckInfo(CheckDto checkDto) {
        checkService.submitCheckInfo(checkDto);
        return ResultObj.ADD_SUCCESS;
    }
}

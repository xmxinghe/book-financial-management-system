package com.chky.book.controller;

import com.chky.book.domain.dto.BookDto;
import com.chky.book.service.IBookService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;

@RestController
@RequestMapping("/book")
public class BookController {

    @Resource
    private IBookService bookService;

    /*加载所有的图书信息*/
    @GetMapping("/loadAllBook")
    public DataGridView loadAllBook(BookDto bookDto) {
        return bookService.list(bookDto);
    }

    /*添加图书信息*/
    @PostMapping("/addBook")
    public ResultObj addBook(BookDto bookDto) {
        bookService.addBook(bookDto);
        return ResultObj.ADD_SUCCESS;
    }

    /*修改图书信息*/
    @RequestMapping("/updateBook")
    public ResultObj updateBook(BookDto bookDto) {
        bookService.updateBook(bookDto);
        return ResultObj.UPDATE_SUCCESS;
    }

    /*删除单个图书信息*/
    @RequestMapping("/deleteBook")
    public ResultObj deleteBook(Integer id) {
        bookService.deleteById(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*批量删除图书信息*/
    @PostMapping("/deleteBatchBook")
    public ResultObj deleteBatch(BookDto bookDto) {
        if (bookDto.getIds() != null && bookDto.getIds().size() != 0) {
            bookService.deleteBatch(bookDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }

    }
}

package com.chky.book.controller;

import com.chky.book.domain.dto.BillDto;
import com.chky.book.service.IBillService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/bill")
public class BillController {

    @Resource
    private IBillService billService;

    /*加载所有的账单信息*/
    @RequestMapping("/loadAllBill")
    public DataGridView loadAllBill(BillDto billDto) {
        return billService.list(billDto);
    }



    /*新增账单信息*/
    @RequestMapping("/addBill")
    public ResultObj addBill(BillDto billDto) {
        billService.addBill(billDto);
        return ResultObj.ADD_SUCCESS;
    }

    /*删除账单信息*/
    @RequestMapping("/deleteBill")
    public ResultObj deleteBill(Integer id) {
        billService.deleteById(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*修改账单信息*/
    @RequestMapping("/updateBill")
    public ResultObj updateBill(BillDto billDto) {
        billService.updateBill(billDto);
        return ResultObj.UPDATE_SUCCESS;
    }

    /*批量删除账单信息*/
    @RequestMapping("/deleteBatchBill")
    public ResultObj deleteBatch(BillDto billDto) {
        if (billDto.getIds() != null && billDto.getIds().size() != 0) {
            billService.deleteBatch(billDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }
    }

    /*导出月报表业务*/
    @RequestMapping("/exportInfo")
    public ResponseEntity<Object> exportMonthInfo() {
        return billService.exportMonthInfo();
    }

    /*导出当前月报表业务*/
    @RequestMapping("/exportMonthInfo")
    public ResponseEntity<Object> export(BillDto billDto) {
        return billService.export(billDto);
    }

    /*根据创建时间加载所有账单信息*/
    @RequestMapping("/loadAllBillByTime")
    public DataGridView loadAllBillByTime(BillDto billDto) {
        return billService.listByTime(billDto);
    }
}

package com.chky.book.controller;

import com.chky.book.domain.dto.StoreDetailDto;
import com.chky.book.service.IStoreService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/store")
public class StoreController {

    @Resource
    private IStoreService storeService;

    /*加载所有仓库信息*/
    @RequestMapping("/loadAllStore")
    public DataGridView loadAllStore(StoreDetailDto storeDetailDto) {
        return storeService.loadAllStore(storeDetailDto);
    }

    /*删除仓库信息*/
    @RequestMapping("/deleteStore")
    public ResultObj deleteStore(Integer id) {
        storeService.deleteById(id);
        return ResultObj.DELETE_SUCCESS;
    }

    /*批量删除仓库信息*/
    @RequestMapping("/deleteBatchStore")
    public ResultObj deleteBatch(StoreDetailDto storeDetailDto) {
        if (storeDetailDto.getIds() != null && storeDetailDto.getIds().size() != 0) {
            storeService.deleteBatch(storeDetailDto.getIds());
            return ResultObj.DELETE_SUCCESS;
        } else {
            return ResultObj.DELETE_CHOICE;
        }
    }

    /*仓库审核业务方法*/
    @RequestMapping("/ProcessStore")
    public ResultObj processStore(StoreDetailDto storeDetailDto) {
        storeService.processStore(storeDetailDto);
        return ResultObj.UPDATE_SUCCESS;
    }
}

package com.chky.book.domain.vo;

import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StoreVo extends BaseContent {
    private Integer page;
    private Integer limit;
    private List<Long> ids;
    private Integer id;  //仓库编号
    private Integer bookId;  //图书编号
    private Integer typeId;  //图书类型编号
    private Integer supplierId;  //供应商编号
    private String supplierName;  //供应商名称
    private String storePerson;  //审核负责人
    private Integer status; //审核状态，0:未审核 1:已审核
    private String bookName;  //图书名称
    private String typeName;  //图书类型
    private Double inPrice;  //进价
    private Double outPrice;  //售价
    private Integer number;  //外购数量
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publicationDate;  //出版时间
    private String profile;  //简介

    private String image; //图书图片
    private String author;  //作者
    private String publishers;  //出版社
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Integer createUser;
    private Integer updateUser;
    private String remark;
}

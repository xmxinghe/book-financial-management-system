package com.chky.book.domain.vo;

import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplierVo extends BaseContent {
    /**
     * 分页参数
     */
    private Integer page;
    private Integer limit;

    //介绍多个图书id
    private List<Long> ids;
    private Integer id;//供应商编号
    private String supplierName; //供应商名称
    private String address;  //供应商地址
    private String telephone;  //联系电话
    private String person;  //负责人
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    //    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Integer createUser;
    private Integer updateUser;
    private String remark;
}

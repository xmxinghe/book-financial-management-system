package com.chky.book.domain.pojo;

import com.chky.system.domain.BaseContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supplier extends BaseContent {
    private Integer id;//供应商编号
    private String supplierName; //供应商名称
    private String address;  //供应商地址
    private String telephone;  //联系电话
    private String person;  //负责人
}

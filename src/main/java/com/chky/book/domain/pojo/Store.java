package com.chky.book.domain.pojo;

import com.chky.system.domain.BaseContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Store extends BaseContent {
    private Integer id;  //仓库编号
    private Integer bookId;  //图书编号
    private Integer typeId;  //图书类型编号
    private Integer supplierId;  //供应商编号
    private String storePerson;  //审核负责人
    private Integer status; //审核状态，0:未审核 1:已审核
    private String bookName;  //图书名称
    private String typeName;  //图书类型
    private Double inPrice;  //进价
    private Double outPrice;  //售价
    private Integer number;  //外购数量
}

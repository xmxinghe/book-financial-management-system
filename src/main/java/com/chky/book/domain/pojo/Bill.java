package com.chky.book.domain.pojo;

import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Bill extends BaseContent {
    private Integer id;  //账单编号
    private String billName;  //账单所属人
    private Integer status;  //账单类型
    private Double amount;  //账单金额
    private String title;  //账单标题
    private Double balance;  //账单余额
    private Integer adminId;  //管理员编号
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;  //报表结束时间
}

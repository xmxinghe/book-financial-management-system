package com.chky.book.domain.pojo;

import com.chky.system.domain.BaseContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Check extends BaseContent {
    private Integer id;  //检查编号
    private String checkInfo;  //检查信息
    private Integer status;  //检查状态，0：磨损；1：丢失
    private Double payMoney;  //赔付金额
    private String checker;  //检查者
    private Integer bookId;  //图书编号
    private Integer adminId;  //管理员编号
}

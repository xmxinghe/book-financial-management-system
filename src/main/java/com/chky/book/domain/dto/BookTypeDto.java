package com.chky.book.domain.dto;

import com.chky.book.domain.pojo.BookType;
import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookTypeDto extends BaseContent {
    /**
     * 分页参数
     */
    private Integer page;
    private Integer limit;

    //介绍多个图书id
    private List<Long> ids;

    private Integer id;
    private String typeName;
}

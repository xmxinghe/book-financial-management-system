package com.chky.book.domain.dto;

import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.aspectj.lang.annotation.AfterThrowing;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PurchaseDto extends BaseContent {
    private Integer page;
    private Integer limit;
    private List<Long> ids;
    private Integer id;  //外购编号
    private String bookName;  //书名
    private Integer supplierId;  //供应商编号
    private Integer inNumber;  //购买数量
    private Double purchasePrice;  //购买单价
    private String purchasePerson;  //购买人
//    private String store;  //仓库名称
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date purchaseDate;  //购货日期
    private String typeName;  //类型名称
//    private Integer bookId;  //图书编号
//    private Integer typeId;  //类型编号
    private String productAddress;  //收货地址
}

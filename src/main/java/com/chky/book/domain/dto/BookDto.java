package com.chky.book.domain.dto;

import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.pojo.BookType;
import com.chky.system.domain.BaseContent;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookDto extends BaseContent {
    /**
     * 分页参数
     */
    private Integer page;
    private Integer limit;

    //介绍多个图书id
    private List<Long> ids;

    private Integer id;  //图书编号
    private String bookName;  //书名
    private Integer typeId;  //图书类型编号
    private String author;  //作者
    private String publishers;  //出版社
    private Double inprice;  //图书进价
    private Double outprice;  //图书售价
    private Integer number;  //图书数量
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date publicationDate;  //出版时间
    private String profile;  //简介

    private String image; //图书图片

    private Integer status;  //是否有库存

//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Integer createUser;
    private Integer updateUser;
    private String remark;
}

package com.chky.book.domain.dto;

import com.chky.system.domain.BaseContent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SupplierDto extends BaseContent {
    /**
     * 分页参数
     */
    private Integer page;
    private Integer limit;

    //介绍多个图书id
    private List<Long> ids;

    private Integer id;//供应商编号
    private String supplierName; //供应商名称
    private String address;  //供应商地址
    private String telephone;  //联系电话
    private String person;  //负责人
}

package com.chky.book.service.impl;

import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.vo.BookTypeVo;
import com.chky.book.domain.vo.BookVo;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.BookTypeMapper;
import com.chky.book.service.IBookTypeService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.WebUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BookTypeServiceImpl implements IBookTypeService {

    @Resource
    private BookTypeMapper bookTypeMapper;

    @Resource
    private BookMapper bookMapper;

    @Override
    public DataGridView loadAllBookType(BookTypeDto bookTypeDto) {
        Page<Object> page = PageHelper.startPage(bookTypeDto.getPage(), bookTypeDto.getLimit());
        List<BookTypeVo> data = bookTypeMapper.list(bookTypeDto);
        return new DataGridView(page.getTotal(), data);
    }

    @Override
    public void add(BookTypeDto bookTypeDto) {
        bookTypeDto.setCreateTime(new Date());
        bookTypeMapper.add(bookTypeDto);
    }

    @Override
    public void deleteById(Integer id) {
        //删除之前判断该类型是否关联了图书信息
//        BookDto bookDto = new BookDto();
//        bookDto.setTypeId(id);
//        List<BookVo> list = bookMapper.list(bookDto);
//        if (list != null) {
//            //说明关联了图书信息，不可删除
//            throw new RuntimeException("该类型已被关联,无法删除");
//        }
//        if (list == null) {
            //没有关联图书信息，根据类型编号删除
            bookTypeMapper.deleteById(id);
//        }
    }

    @Override
    public void updateBookType(BookTypeDto bookTypeDto) {
        bookTypeMapper.update(bookTypeDto);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
//        BookDto bookDto = new BookDto();
//        List<Long> idsDel = new ArrayList<>();
//        ids.forEach(id -> {
            //删除之前判断该类型是否关联了图书信息
//            bookDto.setTypeId(Math.toIntExact(id));
//            List<BookVo> list = bookMapper.list(bookDto);
//            if (list != null) {
                //说明关联了图书信息，不可删除
//                throw new RuntimeException("该类型已被关联,无法删除");
//            }
            //如果没被关联，则放入删除数组
//            idsDel.add(id);
//        });
        if (ids != null) {
            bookTypeMapper.deleteBatch(ids);
        }
    }
}

package com.chky.book.service.impl;

import com.chky.book.domain.dto.BillDto;
import com.chky.book.domain.vo.BillVo;
import com.chky.book.mapper.BillMapper;
import com.chky.book.service.IBillService;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ExportMonthInfoUtils;
import com.chky.system.utils.WebUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;

@Service
public class BillServiceImpl implements IBillService {

    @Resource
    private BillMapper billMapper;
    @Override
    public DataGridView list(BillDto billDto) {
        Page<Object> page = PageHelper.startPage(billDto.getPage(), billDto.getLimit());
        List<BillVo> data = billMapper.list(billDto);
        return new DataGridView(page.getTotal(),data);
    }

    @Override
    public void addBill(BillDto billDto) {
        billDto.setCreateTime(new Date());
        billMapper.addBill(billDto);
    }

    @Override
    public void deleteById(Integer id) {
        billMapper.deleteById(id);
    }

    @Override
    public void updateBill(BillDto billDto) {
        billDto.setCreateTime(new Date());
        billMapper.updateBill(billDto);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null) {
            billMapper.deleteBatch(ids);
        }
    }

    /*导出月报表*/
    @Override
    public ResponseEntity<Object> exportMonthInfo() {
        String fileName = "账单信息报表.xls";
        String sheetName = "账单信息报表";
        //查询所有的账单信息
        List<BillVo> billList = billMapper.list(new BillDto());
        ByteArrayOutputStream bos = ExportMonthInfoUtils.exportMonthInfo(billList, sheetName);
        try {
            //处理文件名称乱码
            fileName = URLEncoder.encode(fileName,"UTF-8");
            //由于要进行下载,所有要设置头信息
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            //设置下载文件的名称
            headers.setContentDispositionFormData("attachment",fileName);
            //将数据组装返回
            return new ResponseEntity<Object>(bos.toByteArray(),headers, HttpStatus.CREATED);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ResponseEntity<Object> export(BillDto billDto) {
        String fileName = "月报表.xls";
        String sheetName = "月报表";
        String start = "";
        String month = "";
        ResponseEntity<Object> response=null;
        if (billDto.getCreateTime() == null) {
            String msg = "请选择月份再导出月报表";
            WebUtils.getHttpServletRequest().setAttribute("msg",msg);
        } else {
            start = billDto.getCreateTime().toLocaleString();
            start = start.substring(0, start.lastIndexOf("-"));
            month = billDto.getCreateTime().toLocaleString();
            month = month.substring(month.indexOf("-") + 1, month.lastIndexOf("-"));
            fileName = start + fileName;
            sheetName = start+ sheetName;
            switch (month) {
                case "1":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "2":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "3":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "4":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "5":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "6":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "7":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "8":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "9":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "10":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "11":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
                case "12":
                    response=exportByMonth(fileName, sheetName, billDto,month);
                    break;
            }
        }
//        String end = billDto.getEndTime().toLocaleString();
//        end = end.substring(0,10);
//        response=exportByMonth(fileName, sheetName, billDto);
        return response;
    }

    @Override
    public DataGridView listByTime(BillDto billDto) {
        String month=null;
        if (billDto.getCreateTime()!=null && !billDto.getCreateTime().equals("")) {
            month = billDto.getCreateTime().toLocaleString();
            month = month.substring(month.indexOf("-") + 1, 7);
        }
        Page<Object> page = PageHelper.startPage(billDto.getPage(), billDto.getLimit());
        List<BillVo> data = billMapper.listByMonth(month);
        return new DataGridView(page.getTotal(),data);
    }

    /*按照月份导出月报表方法*/
    public ResponseEntity<Object> exportByMonth(String fileName,String sheetName,BillDto billDto,String month) {
        //查询所有的账单信息
        List<BillVo> billList = billMapper.listByMonth(month);
        ByteArrayOutputStream bos = ExportMonthInfoUtils.exportMonthInfo(billList, sheetName);
        try {
            //处理文件名称乱码
            fileName = URLEncoder.encode(fileName,"UTF-8");
            //由于要进行下载,所有要设置头信息
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            //设置下载文件的名称
            headers.setContentDispositionFormData("attachment",fileName);
            //将数据组装返回
            return new ResponseEntity<Object>(bos.toByteArray(),headers, HttpStatus.CREATED);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}

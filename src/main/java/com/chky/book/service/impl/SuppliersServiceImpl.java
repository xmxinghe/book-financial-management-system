package com.chky.book.service.impl;

import com.chky.book.domain.dto.SupplierDto;
import com.chky.book.domain.vo.SupplierVo;
import com.chky.book.mapper.SuppliersMapper;
import com.chky.book.service.ISuppliersService;
import com.chky.system.utils.DataGridView;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SuppliersServiceImpl implements ISuppliersService {
    @Resource
    private SuppliersMapper suppliersMapper;

    @Override
    public DataGridView list(SupplierDto supplierDto) {
        Page<Object> page = PageHelper.startPage(supplierDto.getPage(), supplierDto.getLimit());
        List<SupplierVo> data=suppliersMapper.list(supplierDto);
        return new DataGridView(page.getTotal(),data);
    }

    @Override
    public void addSupplier(SupplierDto supplierDto) {
        supplierDto.setCreateTime(new Date());
        suppliersMapper.add(supplierDto);
    }

    @Override
    public void deleteById(Integer id) {
        suppliersMapper.deleteById(id);
    }

    @Override
    public void updateSupplier(SupplierDto supplierDto) {
        supplierDto.setCreateTime(new Date());
        suppliersMapper.updateSupplier(supplierDto);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null) {
            suppliersMapper.deleteBatch(ids);
        }
    }
}

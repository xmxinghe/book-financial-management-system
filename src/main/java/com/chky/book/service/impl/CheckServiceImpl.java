package com.chky.book.service.impl;

import com.chky.book.domain.dto.BillDto;
import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.dto.CheckDto;
import com.chky.book.domain.pojo.Bill;
import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.pojo.Store;
import com.chky.book.mapper.BillMapper;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.CheckMapper;
import com.chky.book.mapper.StoreMapper;
import com.chky.book.service.ICheckService;
import com.chky.system.utils.WebUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class CheckServiceImpl implements ICheckService {
    @Resource
    private CheckMapper checkMapper;
    @Resource
    private BookMapper bookMapper;

    @Resource
    private StoreMapper storeMapper;

    @Resource
    private BillMapper billMapper;

    @Transactional(readOnly = false)
    @Override
    public void submitCheckInfo(CheckDto checkDto) {
        //根据图书编号查询图书信息
        Book book = bookMapper.getById(checkDto.getBookId());
        //1.判断检查状态，0：磨损；1：丢失
        if (checkDto.getStatus() == 0) {
            //磨损：磨损则赔付十元并将书籍信息录入图书仓库
            //1.设置赔付金额为十元
            checkDto.setPayMoney(10D);
            //2.图书流水中插入一条信息
            BillDto bill = new BillDto();
            bill.setCreateTime(new Date());
            bill.setStatus(1);  //0：支出  1：收入
            bill.setTitle("图书报损:磨损");  //设置账单标题
            bill.setBillName(checkDto.getChecker());
            bill.setAmount(10D);
            billMapper.addBill(bill);
            //3.更新图书信息
            BookDto bookDto = new BookDto();
            BeanUtils.copyProperties(book,bookDto);
            bookDto.setNumber(book.getNumber()+1);
            book = bookMapper.getById(book.getId());
            if (book.getNumber() <= 0) {
                bookDto.setStatus(1);  //0:有库存 1：无库存
            }
            bookMapper.updateBook(bookDto);
        } else if (checkDto.getStatus() == 1) {
            //丢失：赔付书籍售价金额，并录入到图书流水中
            //1.设置赔付金额未图书售价
            checkDto.setPayMoney(book.getOutprice());
            //2.图书流水中插入一条信息
            BillDto bill = new BillDto();
            bill.setCreateTime(new Date());
            bill.setStatus(1);  //0：支出  1：收入
            bill.setTitle("图书报损:丢失");
            bill.setBillName(checkDto.getChecker());
            bill.setAmount(book.getOutprice());
            billMapper.addBill(bill);
        }
        // 2.插入检查信息
        checkMapper.addCheckInfo(checkDto);
    }
}

package com.chky.book.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.vo.BookVo;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.BookTypeMapper;
import com.chky.book.service.IBookService;
import com.chky.system.constant.SysConstant;
import com.chky.system.utils.AppFileUtils;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.WebUtils;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class BookServiceImpl implements IBookService {

    @Resource
    private BookMapper bookMapper;

    @Resource
    private BookTypeMapper bookTypeMapper;

    @Override
    public DataGridView list(BookDto bookDto) {
        Page<Object> page = PageHelper.startPage(bookDto.getPage(), bookDto.getLimit());
        List<BookVo> data=bookMapper.list(bookDto);
        return new DataGridView(page.getTotal(),data);
    }

    @Override
    public void addBook(BookDto bookDto) {
        bookDto.setCreateTime(new Date());
        bookDto.setStatus(0);
        if (bookDto.getImage() != null) {
            //如果不是默认图片，就去的掉图片的_temp的后缀
            if (!bookDto.getImage().equals(SysConstant.DEFAULT_Book_IMG)) {
                String filePath = AppFileUtils.updateFileName(bookDto.getImage(), SysConstant.FILE_UPLOAD_TEMP);
                bookDto.setImage(filePath);
            }
        }
        bookMapper.addBook(bookDto);
    }

    @Override
    public void updateBook(BookDto bookDto) {
        String image = bookDto.getImage();
        if (bookDto.getImage() != null) {
            //如果图片路径中含有_temp,则删除
            if (image.endsWith(SysConstant.FILE_UPLOAD_TEMP)) {
                String filePath = AppFileUtils.updateFileName(bookDto.getImage(), SysConstant.FILE_UPLOAD_TEMP);
                bookDto.setImage(filePath);
                //把原来的删除
                Book book =bookMapper.getById(bookDto.getId());
                if (book != null) {
                    AppFileUtils.removeFileByPath(book.getImage());
                }
            }
        }
        bookMapper.updateBook(bookDto);
    }

    @Override
    public void deleteById(Integer id) {
        bookMapper.deleteById(id);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null) {
            bookMapper.deleteBatch(ids);
        }
    }
}

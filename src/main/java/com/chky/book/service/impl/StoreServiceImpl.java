package com.chky.book.service.impl;

import com.chky.book.domain.dto.*;
import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.vo.StoreVo;
import com.chky.book.mapper.BillMapper;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.BookTypeMapper;
import com.chky.book.mapper.StoreMapper;
import com.chky.book.service.IStoreService;
import com.chky.system.utils.DataGridView;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class StoreServiceImpl implements IStoreService {
    @Resource
    private StoreMapper storeMapper;

    @Resource
    private BookMapper bookMapper;

    @Resource
    private BookTypeMapper bookTypeMapper;

    @Resource
    private BillMapper billMapper;

    @Override
    public DataGridView loadAllStore(StoreDetailDto storeDetailDto) {
        //查询图书名称是否存在
        Page<Object> page = PageHelper.startPage(storeDetailDto.getPage(), storeDetailDto.getLimit());
        List<StoreVo> data = storeMapper.list(storeDetailDto);
        return new DataGridView(page.getTotal(), data);
    }

    /*图书审核业务*/
    @Transactional(readOnly = false)
    @Override
    public void processStore(StoreDetailDto storeDetailDto) {
        //根据图书名查询图书信息
        Book book = bookMapper.getByName(storeDetailDto.getBookName());
        //根据图书类型名称查询图书类型信息
        BookType type = bookTypeMapper.getByName(storeDetailDto.getTypeName());
        //创建dto对象
        BookDto bookDto = new BookDto();
        BookTypeDto typeDto = new BookTypeDto();
        BillDto billDto = new BillDto();
        StoreDto storeDto = new StoreDto();
        //判断状态是否未已审核 0:未审核 1:已审核
        if (storeDetailDto.getStatus() == 0) {
            //1.判断图书编号是否存在
            if (book!=null) {
                //图书编号一致
                //该类型和该图书名已存在，更新图书数量，并更新图书相关信息
                BeanUtils.copyProperties(book, bookDto);
                //更新图书数量为当前数量加此次审核通过的图书数量
                bookDto.setNumber(book.getNumber() + storeDetailDto.getNumber());
                //查询该图书对象信息，判断库存是否为空
                book = bookMapper.getById(bookDto.getId());
                if (book.getNumber() == 0) {
                    //无库存，设置bookDto库存状态
                    bookDto.setStatus(1);
                }
                //有库存，设置bookDto库存状态
                bookDto.setStatus(0);
                bookDto.setCreateTime(new Date());
                bookMapper.updateBook(bookDto);  //审核通过，更新该图书信息
                //更新审核状态为已审核
                BeanUtils.copyProperties(storeDetailDto, storeDto);
                storeDto.setStatus(1);
                storeMapper.update(storeDto);
            }
            //2.该图书不存在，是新书，插入该新书信息
            //判断图书类型编号是否一致
            if (type != null) {
                //图书类型编号一致
                //根据dto，插入新的图书信息
                bookDto.setBookName(storeDetailDto.getBookName());  //设置图书名称
                bookDto.setTypeId(type.getId());  //设置类型编号
//            bookDto.setInprice(storeDetailDto.getInPrice());
                bookDto.setOutprice(storeDetailDto.getOutPrice());  //设置售价
                bookDto.setImage(storeDetailDto.getImage());  //设置图书图片
                bookDto.setNumber(storeDetailDto.getNumber());  //设置图书数量
                bookDto.setAuthor(storeDetailDto.getAuthor());  //设置作者
                bookDto.setPublicationDate(storeDetailDto.getPublicationDate());  //设置出版日期
                bookDto.setStatus(0);  //设置库存状态  0：有库存 1：无库存
                bookDto.setCreateTime(new Date());
                bookDto.setProfile(storeDetailDto.getProfile());  //设置图书简介
                bookDto.setPublishers(storeDetailDto.getPublishers());  //设置图书出版社
                bookMapper.addBook(bookDto);  //新增图书信息
                //更新审核状态为已审核
                BeanUtils.copyProperties(storeDetailDto, storeDto);
                storeDto.setStatus(1);
                storeMapper.update(storeDto);
            } else {
                //3.图书类型和图书名称都不存在
                //新增该图书类型
                typeDto.setTypeName(storeDetailDto.getTypeName());
                typeDto.setCreateTime(new Date());
                bookTypeMapper.add(typeDto);
                //根据dto，插入新的图书信息
                bookDto.setBookName(storeDetailDto.getBookName());  //设置图书名称
                bookDto.setTypeId(typeDto.getId());  //设置类型编号
                bookDto.setOutprice(storeDetailDto.getOutPrice());  //设置售价
                bookDto.setImage(storeDetailDto.getImage());  //设置图书图片
                bookDto.setNumber(storeDetailDto.getNumber());  //设置图书数量
                bookDto.setAuthor(storeDetailDto.getAuthor());  //设置作者
                bookDto.setPublicationDate(storeDetailDto.getPublicationDate());  //设置出版日期
                bookDto.setStatus(0);  //设置库存状态  0：有库存 1：无库存
                bookDto.setCreateTime(new Date());
                bookDto.setProfile(storeDetailDto.getProfile());  //设置图书简介
                bookDto.setPublishers(storeDetailDto.getPublishers());  //设置图书出版社
                bookMapper.addBook(bookDto);  //新增图书信息
                //更新审核状态为已审核
                BeanUtils.copyProperties(storeDetailDto, storeDto);
                storeDto.setStatus(1);
                storeMapper.update(storeDto);
            }
            //4.审核通过，新增账单信息
            billDto.setBillName(storeDetailDto.getStorePerson());
            billDto.setTitle("图书外购:" + storeDetailDto.getBookName());
            billDto.setStatus(0);  //账单状态类型:0:支出 1：收入
            billDto.setAmount(storeDetailDto.getInPrice() * storeDetailDto.getNumber());
            billDto.setCreateTime(new Date());
            billMapper.addBill(billDto);  //新增账单信息
        } else if (storeDetailDto.getStatus() == 1) {
            //5.审核未通过 0:未审核 1:已审核
            //更新图书仓库相关信息
            BeanUtils.copyProperties(storeDetailDto, storeDto);
            storeMapper.update(storeDto);
        }
    }

    @Override
    public void deleteById(Integer id) {
        //判断是否已审核，未审核则不能删除
        storeMapper.deleteById(id);
    }

    @Override
    public void deleteBatch(List<Long> ids) {
        if (ids != null) {
            storeMapper.deleteBatch(ids);
        }
    }
}

package com.chky.book.service.impl;

import cn.hutool.core.date.DateTime;
import com.chky.book.domain.dto.BookDto;
import com.chky.book.domain.dto.BookTypeDto;
import com.chky.book.domain.dto.PurchaseDto;
import com.chky.book.domain.pojo.Book;
import com.chky.book.domain.pojo.BookType;
import com.chky.book.domain.pojo.Purchase;
import com.chky.book.domain.pojo.Store;
import com.chky.book.mapper.BookMapper;
import com.chky.book.mapper.BookTypeMapper;
import com.chky.book.mapper.PurchaseMapper;
import com.chky.book.mapper.StoreMapper;
import com.chky.book.service.IPurchaseService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

@Service
public class PurchaseServiceImpl implements IPurchaseService {
    @Resource
    private StoreMapper storeMapper;

    @Resource
    private PurchaseMapper purchaseMapper;

    @Resource
    private BookMapper bookMapper;

    @Resource
    private BookTypeMapper bookTypeMapper;
    @Transactional(readOnly = false)
    @Override
    public void submitPurchaseInfo(PurchaseDto purchaseDto) {
        Store store = new Store();
        //判断图书名称是否已经在仓库中存在,且未审核
        //存在则更新数量和入库时间
        Store tempStore=storeMapper.getByName(purchaseDto.getBookName());
        if (tempStore != null && tempStore.getStatus()==0) {
            //该书名存在且未审核,更新该图书数量
            tempStore.setNumber(tempStore.getNumber()+purchaseDto.getInNumber());
            tempStore.setCreateTime(new Date());
            storeMapper.updateStore(tempStore);
        }
        //判断该图书名称是否存在
        Book book = new Book();
        book=bookMapper.getByName(purchaseDto.getBookName());
        if (book != null) {
            //存在该图书，将图书编号赋给图书仓库
            store.setBookId(book.getId());
        }
        //判断该图书类型是否存在
        BookType bookType = new BookType();
        bookType = bookTypeMapper.getByName(purchaseDto.getTypeName());
        if (bookType != null) {
            //存在该图书类型，类型编号赋给图书仓库
            store.setTypeId(bookType.getId());
        }
        //将信息存入图书审核仓库中
        store.setStatus(0);  //未审核
        store.setStorePerson(purchaseDto.getPurchasePerson());  //设置操作人
        store.setBookName(purchaseDto.getBookName());  //设置图书名称
        store.setTypeName(purchaseDto.getTypeName());  //设置类型名称
        store.setInPrice(purchaseDto.getPurchasePrice());  //设置该图书进价
        store.setNumber(purchaseDto.getInNumber());  //设置外购数量
        store.setCreateTime(new DateTime());
        store.setSupplierId(purchaseDto.getSupplierId());  //设置供应商编号
        storeMapper.addStoreInfo(store);
        //2.将信息存入图书外购表中
        Purchase purchase = new Purchase();
        BeanUtils.copyProperties(purchaseDto,purchase);
        purchase.setPurchaseDate(new Date());
        purchaseMapper.addPurchaseInfo(purchase);
    }
}

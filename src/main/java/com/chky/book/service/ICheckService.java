package com.chky.book.service;

import com.chky.book.domain.dto.CheckDto;

public interface ICheckService {
    void submitCheckInfo(CheckDto checkDto);
}

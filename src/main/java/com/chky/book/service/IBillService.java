package com.chky.book.service;

import com.chky.book.domain.dto.BillDto;
import com.chky.system.utils.DataGridView;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IBillService {
    DataGridView list(BillDto billDto);

    void addBill(BillDto billDto);

    void deleteById(Integer id);

    void updateBill(BillDto billDto);

    void deleteBatch(List<Long> ids);

    ResponseEntity<Object> exportMonthInfo();

    ResponseEntity<Object> export(BillDto billDto);

    DataGridView listByTime(BillDto billDto);
}

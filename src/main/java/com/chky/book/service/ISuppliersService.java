package com.chky.book.service;

import com.chky.book.domain.dto.SupplierDto;
import com.chky.system.utils.DataGridView;

import java.util.List;

public interface ISuppliersService {
    DataGridView list(SupplierDto supplierDto);

    void addSupplier(SupplierDto supplierDto);

    void deleteById(Integer id);

    void updateSupplier(SupplierDto supplierDto);

    void deleteBatch(List<Long> ids);
}

package com.chky.book.service;

import com.chky.book.domain.dto.BookDto;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;

import java.util.List;

public interface IBookService {
    DataGridView list(BookDto bookDto);

    void addBook(BookDto bookDto);

    void updateBook(BookDto bookDto);

    void deleteById(Integer id);

    void deleteBatch(List<Long> ids);
}

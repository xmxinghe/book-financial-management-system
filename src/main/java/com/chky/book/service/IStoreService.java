package com.chky.book.service;

import com.chky.book.domain.dto.StoreDetailDto;
import com.chky.system.utils.DataGridView;
import com.chky.system.utils.ResultObj;

import java.util.List;

public interface IStoreService {
    DataGridView loadAllStore(StoreDetailDto storeDetailDto);

    void processStore(StoreDetailDto storeDetailDto);

    void deleteById(Integer id);

    void deleteBatch(List<Long> ids);
}

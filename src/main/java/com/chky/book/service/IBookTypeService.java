package com.chky.book.service;

import com.chky.book.domain.dto.BookTypeDto;
import com.chky.system.utils.DataGridView;

import java.util.List;

public interface IBookTypeService {
    DataGridView loadAllBookType(BookTypeDto bookTypeDto);

    void add(BookTypeDto bookTypeDto);

    void deleteById(Integer id);

    void updateBookType(BookTypeDto bookTypeDto);

    void deleteBatch(List<Long> ids);
}

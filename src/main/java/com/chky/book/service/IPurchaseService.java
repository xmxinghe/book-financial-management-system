package com.chky.book.service;

import com.chky.book.domain.dto.PurchaseDto;

public interface IPurchaseService {
    void submitPurchaseInfo(PurchaseDto purchaseDto);
}

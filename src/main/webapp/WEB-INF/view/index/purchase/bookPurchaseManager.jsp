<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>图书外购</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="icon" href="favicon.ico">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/public.css" media="all"/>
</head>
<body class="childrenBody">

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>图书外购</legend>
</fieldset>
<form class="layui-form" method="post" id="purchaseFrm">
    <div class="layui-form-item">
        <div class="layui-inline" hidden>
            <%--            <label class="layui-form-label" hidden>检查编号:</label>--%>
            <div class="layui-input-block" style="padding: 5px" hidden>
                <input type="text" name="id" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入外购编号" style="height: 30px;border-radius: 10px">
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">供应商名称:</label>
            <div class="layui-input-block" style="padding: 5px">
                <select name="supplierName" id="supplierName" lay-verify="required" lay-filter="supplierName">
                    <option value="">请选择</option>
                    <c:forEach items="${sessionScope.supplierList}" var="list">
                        <option value="${list.id}">${list.supplierName}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">图书名称:</label>
            <div class="layui-input-block" style="padding: 5px">
                <div class="layui-input-inline" style="padding: 5px">
                    <input type="text" name="bookName" id="bookName" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入外购图书名称" lay-verify="required"
                           style="height: 30px;border-radius: 10px">
                </div>
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">图书类型:</label>
            <div class="layui-input-block" style="padding: 5px">
                <div class="layui-input-inline" style="padding: 5px">
                    <input type="text" name="typeName" id="typeName" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入图书类型" lay-verify="required" style="height: 30px;border-radius: 10px">
                </div>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-block">
            <label class="layui-form-label">图书单价:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="purchasePrice" id="purchasePrice" autocomplete="off"
                       class="layui-input layui-input-inline"
                       placeholder="请输入图书单价" lay-verify="required" style="height: 30px;border-radius: 10px"><br>
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">购买数量:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="inNumber" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入购买数量" lay-verify="required" style="height: 30px;border-radius: 10px"><br>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-block">
                <label class="layui-form-label">进货员名称:</label>
                <div class="layui-input-inline" style="padding: 5px">
                    <input type="text" name="purchasePerson" id="purchasePerson" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入进货员名称" lay-verify="required"
                           style="height: 30px;border-radius: 10px"><br>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-block">
                <label class="layui-form-label">收货地址:</label>
                <div class="layui-input-inline">
                    <input type="text" name="productAddress" id="productAddress" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入收货地址" lay-verify="required" style="height: 30px;border-radius: 10px">
                </div>
            </div>
        </div>
        <%--        <div class="layui-form-item">--%>
        <%--            <div class="layui-block">--%>
        <%--                <label class="layui-form-label">所属仓库:</label>--%>
        <%--                <div class="layui-input-inline">--%>
        <%--                    <input type="text" name="store"  autocomplete="off"--%>
        <%--                           class="layui-input layui-input-inline"--%>
        <%--                           placeholder="请输入所属仓库" style="height: 30px;border-radius: 10px">--%>
        <%--                </div>--%>
        <%--            </div>--%>
        <%--        </div>--%>
        <div class="layui-form-item">
            <div class="layui-block" hidden>
                <label class="layui-form-label" hidden>外购日期:</label>
                <div class="layui-input-inline">
                    <input type="text" name="purchaseDate" id="purchaseDate" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入外购日期" style="height: 30px;border-radius: 10px">
                </div>
            </div>
        </div>

        <div class="layui-block">
            <button type="button"
                    class="layui-btn layui-btn-normal layui-icon layui-icon-search layui-btn-radius layui-btn-sm"
                    id="doPurchase" style="margin-top: 4px;height: 30px;border-radius: 10px">提交
            </button>
            <button type="reset"
                    class="layui-btn layui-btn-warm layui-icon layui-icon-refresh layui-btn-radius layui-btn-sm"
                    style="margin-top: 4px;height: 30px;border-radius: 10px">重置
            </button>
        </div>
    </div>
</form>


<script src="${pageContext.request.contextPath}/resources/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['jquery', 'layer', 'form', 'table', 'laydate'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        laydate.render({
            elem: 'purchaseDate',
            type: 'datetime'
        })


        //提交检查信息
        $("#doPurchase").click(function () {
            //获取下拉框参数
            var supplierId = $('#supplierName option:selected').val();
            var bookName = $('#bookName').val();
            var typeName = $('#typeName').val();
            var purchasePrice = $('#purchasePrice').val();
            var inNumber = $('#inNumber').val();
            var purchasePerson = $('#purchasePerson').val();
            var productAddress = $('#productAddress').val();
            // var purchaseDate = $('#purchaseDate').val();
            //获取参数
            // console.log("type=" + typeId);
            //获取表单的参数
            var params = $("#purchaseFrm").serialize();
            params += "&supplierId=" + supplierId;
            // alert("params=" + params);
            // if ((supplierId == null || supplierId == "" || supplierId.length == 0) ||
            //     (bookName == null || bookName == "" || bookName.length == 0) ||
            //     (typeName == null || typeName == "" || typeName.length == 0) ||
            //     (purchasePrice == null || purchasePrice == 0) ||
            //     (inNumber == null || inNumber == 0 ) ||
            //     (purchasePerson == null || purchasePerson == "" || purchasePerson.length == 0) ||
            //     (productAddress == null || productAddress == "" || productAddress.length == 0)
            // ) {
            //     layer.msg("请将图书外购信息填写完整");
            // } else {
                $.post("${pageContext.request.contextPath}/purchase/submitPurchaseInfo.action", params, function (result) {
                    layer.msg(result.data);
                    layer.msg("提交成功，已入库...");
                    var url = "${pageContext.request.contextPath}/index/toBookStoreManager.action";
                    window.location.href = url;
                });
            // }
        })
    })
</script>
</body>
</html>


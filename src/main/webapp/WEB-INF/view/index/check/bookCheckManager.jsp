<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>图书报损</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="icon" href="favicon.ico">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/public.css" media="all"/>
</head>
<body class="childrenBody">

<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>图书报损</legend>
</fieldset>
<form class="layui-form" method="post" id="checkFrm">
    <div class="layui-form-item">
        <div class="layui-inline" hidden>
            <%--            <label class="layui-form-label" hidden>检查编号:</label>--%>
            <div class="layui-input-block" style="padding: 5px" hidden>
                <input type="text" name="id" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入检查编号" style="height: 30px;border-radius: 10px">
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">检查信息:</label>
            <div class="layui-input-block" style="padding: 5px">
                <input type="text" name="checkInfo" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入检查信息" style="height: 30px;border-radius: 10px" lay-verify="required"
                       id="checkInfo">
            </div>
        </div>
        <div class="layui-block">
            <label class="layui-form-label">图书名称:</label>
            <div class="layui-input-block" style="padding: 5px">
                <select name="bookName" id="bookName" lay-verify="required" lay-filter="bookFilter">
                    <option value="">请选择</option>
                    <c:forEach items="${sessionScope.bookList}" var="list">
                        <option value="${list.id}">${list.bookName} 售价:${list.outprice}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-block" hidden>
            <label class="layui-form-label">赔付金额:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="payMoney" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入赔付金额" style="height: 30px;border-radius: 10px" lay-verify="required"><br>
                <tip>注:丢失则原价赔付,其他情况缴纳文献加工费十元并归还图书</tip>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-block">
                <label class="layui-form-label">检查状态:</label>
                <div class="layui-input-inline">
                    <input type="radio" name="status" value="0" title="磨损" checked="checked">
                    <input type="radio" name="status" value="1" title="丢失"><br>
                    <tip>注:丢失则原价赔付,其他情况缴纳文献加工费十元并归还图书</tip>
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-block">
                <label class="layui-form-label">检查者:</label>
                <div class="layui-input-inline" readonly="">
                    <input type="text" name="checker" value="${sessionScope.user.adminName}" autocomplete="off"
                           class="layui-input layui-input-inline"
                           placeholder="请输入检查者" lay-verify="required" style="height: 30px;border-radius: 10px"
                           id="checker">
                </div>
            </div>
        </div>

        <div class="layui-block">
            <button type="button"
                    class="layui-btn layui-btn-normal layui-icon layui-icon-search layui-btn-radius layui-btn-sm"
                    id="doCheck" style="left: auto;margin-top: 4px;height: 30px;border-radius: 10px">提交
            </button>
            <button type="reset"
                    class="layui-btn layui-btn-warm layui-icon layui-icon-refresh layui-btn-radius layui-btn-sm"
                    style="left: auto;margin-top: 4px;height: 30px;border-radius: 10px">重置
            </button>
        </div>
    </div>
</form>


<script src="${pageContext.request.contextPath}/resources/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['jquery', 'layer', 'form', 'table'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;


        //提交检查信息
        $("#doCheck").click(function () {
                //获取下拉框参数
                var bookId = $('#bookName option:selected').val();
                var checkInfo = $('#checkInfo').val();
                var checker = $('#checker').val();
                //获取参数
                // console.log("type=" + typeId);
                //获取表单的参数
                var params = $("#checkFrm").serialize();
                params += "&bookId=" + bookId;
                // alert("params=" + params);
                if ((bookId == null || bookId == "" || bookId.length == 0) ||
                    (checkInfo == null || checkInfo == "" || checkInfo.length == 0) ||
                    (checker == null || checker == "" || checker.length == 0)
                ) {
                    layer.msg("请将报损信息填写完整");
                } else {
                    $.post("${pageContext.request.contextPath}/check/submitCheckInfo.action", params, function (result) {
                        layer.msg(result.data)
                        layer.msg("提交成功，正在前往图书流水...");
                        //跳转至图书流水页面
                        var url = "${pageContext.request.contextPath}/index/toBookBillManager.action"
                        window.location.href = url;
                    });
                }
            }
        )
    })

</script>
</body>
</html>


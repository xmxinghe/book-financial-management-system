<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>图书仓库</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="icon" href="favicon.ico">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/public.css" media="all"/>
</head>
<body class="childrenBody">

<!-- 搜索条件开始 -->
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查询条件</legend>
</fieldset>
<form class="layui-form" method="post" id="searchFrm">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">图书名称:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="bookName" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入书名" style="height: 30px;border-radius: 10px">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">图书类型:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="typeName" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入图书类型" style="height: 30px;border-radius: 10px">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">供应商名称:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <select name="supplierName" id="supplierName" lay-verify="required" lay-filter="supplierName">
                    <option value="">请选择</option>
                    <c:forEach items="${sessionScope.supplierList}" var="list">
                        <option  value="${list.id}">${list.supplierName}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">图书进价:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="inPrice" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入图书进价" style="height: 30px;border-radius: 10px">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">外购数量:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="number" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入外购数量" style="height: 30px;border-radius: 10px">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">是否审核:</label>
            <div class="layui-input-inline">
                <input type="radio" name="status" value="0" title="未审核">
                <input type="radio" name="status" value="1" title="已审核">
            </div>
            <button type="button"
                    class="layui-btn layui-btn-normal layui-icon layui-icon-search layui-btn-radius layui-btn-sm"
                    id="doSearch" style="margin-top: 4px">查询
            </button>
            <button type="reset"
                    class="layui-btn layui-btn-warm layui-icon layui-icon-refresh layui-btn-radius layui-btn-sm"
                    style="margin-top: 4px">重置
            </button>
        </div>
    </div>

</form>

<!-- 数据表格开始 -->
<table class="layui-hide" id="storeTable" lay-filter="storeTable"></table>
<div id="storeToolBar" style="display: none;">
<%--    <button type="button" class="layui-btn layui-btn-sm layui-btn-radius" lay-event="add">增加</button>--%>
    <button type="button" class="layui-btn layui-btn-danger layui-btn-sm layui-btn-radius" lay-event="deleteBatch">
        批量删除
    </button>
</div>
<div id="storeBar" style="display: none;">
    <a class="layui-btn layui-btn-xs layui-btn-radius" lay-event="edit">审核</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs layui-btn-radius" lay-event="del">删除</a>
</div>

<!-- 添加和修改的弹出层-->
<div style="display: none;padding: 20px" id="saveOrUpdateDiv">
    <form class="layui-form layui-row layui-col-space10" lay-filter="dataFrm" id="dataFrm">
        <div class="layui-col-md12 layui-col-xs12">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md9 layui-col-xs7">
                    <div class="layui-form-item magt3" hidden>
                        <label class="layui-form-label">仓库编号:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="id" id="id" autocomplete="off" class="layui-input"
                                   placeholder="请输入仓库编号" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item magt3" hidden>
                        <label class="layui-form-label">图书编号:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="bookId" id="bookId" autocomplete="off" class="layui-input"
                                   placeholder="请输入图书编号" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item magt3" hidden>
                        <label class="layui-form-label">类型编号:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="typeId" id="typeId" autocomplete="off" class="layui-input"
                                   placeholder="请输入类型编号" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item magt3">
                        <label class="layui-form-label">图书名称:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="bookName" id="bookName" autocomplete="off" class="layui-input"
                                   lay-verify="required"
                                   placeholder="请输入图书名称" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">图书类型:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="typeName" id="typeName" autocomplete="off" class="layui-input"
                                   lay-verify="required"
                                   placeholder="请输入类型名称" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">作者:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="author" autocomplete="off" class="layui-input"
                                   placeholder="请输入图书作者" lay-verify="required" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                </div>
                <div class="layui-col-md3 layui-col-xs5">
                    <div class="layui-upload-list thumbBox mag0 magt3" id="bookImgDiv">
                        <%--显示要上传的图片--%>
                        <img class="layui-upload-img thumbImg" id="showBookImg">
                        <%--保存当前显示图片的地址--%>
                        <input type="hidden" name="image" id="image">
                    </div>
                </div>
            </div>
            <div class="layui-form-item magb0">
                <label class="layui-form-label">出版社:</label>
                <div class="layui-input-block" style="padding: 5px">
                    <input type="text" name="publishers" autocomplete="off" class="layui-input"
                           placeholder="请输入出版社" lay-verify="required" style="height: 30px;border-radius: 10px">
                </div>
            </div>

            <div class="layui-form-item magb0">
                <div class="layui-inline">
                    <label class="layui-form-label">图书进价:</label>
                    <div class="layui-input-block" style="padding: 5px">
                        <input type="text" name="inPrice" class="layui-input" lay-verify="required|number"
                               placeholder="请输入图书进价" style="height: 30px;border-radius: 10px" readonly>
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">图书售价:</label>
                    <div class="layui-input-block" style="padding: 5px">
                        <input type="text" name="outPrice" class="layui-input" lay-verify="required|number"
                               placeholder="请输入图书售价" lay-verify="required" style="height: 30px;border-radius: 10px">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">外购数量:</label>
                    <div class="layui-input-block" style="padding: 5px">
                        <input type="text" name="number" class="layui-input" lay-verify="required|number"
                               placeholder="请输入图书数量" lay-verify="required" style="height: 30px;border-radius: 10px" readonly>
                    </div>
                </div>
                <div class="layui-form-item magb0">
                    <label class="layui-form-label">图书简介:</label>
                    <div class="layui-input-inline" style="padding: 5px">
                        <input type="text" name="profile" autocomplete="off" class="layui-input"
                               placeholder="请输入图书简介" lay-verify="required" style="height: 30px;border-radius: 10px">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">是否已审核:</label>
                    <div class="layui-input-inline">
                        <input type="radio" name="status" value="0" title="未审核" checked readonly>
                        <input type="radio" name="status" value="1" title="已审核" readonly>
                    </div>
                </div>
            </div>
            <div class="layui-form-item magb0">
                <div class="layui-inline">
                    <label class="layui-form-label">出版日期:</label>
                    <div class="layui-input-inline" style="padding: 5px">
                        <input type="text" name="publicationDate" id="publicationDate" class="layui-input"
                               lay-verify="required"
                               placeholder="请输入出版日期" lay-verify="required" style="height: 30px;border-radius: 10px">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">仓库审核员:</label>
                    <div class="layui-input-inline" style="padding: 5px">
                        <input type="text" name="storePerson" id="storePerson" class="layui-input"
                               lay-verify="required"
                               placeholder="请输入仓库审核员" lay-verify="required" style="height: 30px;border-radius: 10px" readonly>
                    </div>
                </div>
            </div>
            <div class="layui-form-item magb0">
                <div class="layui-input-block" style="text-align: center;padding-right: 120px">
                    <button type="button"
                            class="layui-btn layui-btn-normal layui-btn-md layui-icon layui-icon-release layui-btn-radius"
                            lay-filter="doSubmit" lay-submit="">提交
                    </button>
                    <button type="reset"
                            class="layui-btn layui-btn-warm layui-btn-md layui-icon layui-icon-refresh layui-btn-radius">
                        重置
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>


<script src="${pageContext.request.contextPath}/resources/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['jquery', 'layer', 'form', 'table', 'upload', 'laydate'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var upload = layui.upload;
        var laydate = layui.laydate;

        //渲染日期表格
        laydate.render({
            elem: '#publicationDate',
            type: 'datetime'
        })


        //渲染数据表格
        tableIns = table.render({
            elem: '#storeTable', //渲染的表格对象
            url: '${pageContext.request.contextPath}/store/loadAllStore.action', //请求数据接口的地址
            title: '图书仓库数据表',
            toolbar: '#storeToolBar',
            height: 'full-205',
            cellMinWidth: 100, //设置列的最小默认宽度
            page: true, //开启分页
            cols: [[
                {type: 'checkbox', fixed: 'left'},
                {field: 'id', title: '仓库编号', align: 'center', width: '100'},
                {field: 'bookName', title: '图书名称', align: 'center', width: '100'},
                {field: 'typeName', title: '图书类型', align: 'center', width: '100'},
                {field: 'inPrice', title: '图书进价', align: 'center', width: '90'},
                {field: 'number', title: '外购数量', align: 'center', width: '90'},
                {field: 'storePerson', title: '仓库负责人', align: 'center', width: '90'},
                {
                    field: 'status', title: '是否已审核', align: 'center', width: '150', templet: function (d) {
                        return d.status == '1' ? '<font color=#adff2f>已审核</font>' : '<font color=red>未审核</font>'
                    }
                },
                {field: 'supplierName', title: '供应商名称', align: 'center', width: '120'},
                {field: 'createTime', title: '入库时间', align: 'center', width: '160'},
                {fixed: 'right', title: '操作', toolbar: '#storeBar', align: 'center', width: '180'}
            ]]
            /*            ,
                        done: function (data,curr ,count){
                            //不是第一页时,如果返回当前返回值的数据为0,那么我们就返回上一页
                            if(data.data.length == 0 && curr != 1){
                                tableIns.reload({
                                    page: {
                                        curr: curr - 1
                                    }
                                })
                            }
                        }*/
        })

        //模糊按条件查询
        $("#doSearch").click(function () {
            //获取下拉框参数
            var supplierId = $('#supplierName option:selected').val();
            // console.log("type=" + typeId);
            //获取表单的参数
            var params = $("#searchFrm").serialize();
            params += "&supplierId=" + supplierId;
            // alert("params=" + params);
            tableIns.reload({
                url: "${pageContext.request.contextPath}/store/loadAllStore.action?" + params,
                page: {curr: 1}
                })
            }
        )

        var mainIndex;
        var url;


        //监听行工具栏
        table.on('tool(storeTable)', function (obj) {
            //获取当前的行数据
            var data = obj.data;
            //获取事件
            var layEvent = obj.event;
            if (layEvent == 'del') {
                layer.confirm("是否确认删除[" + data.id + "]这条仓库信息?", function (index) {
                    //发送ajax请求
                    $.get("${pageContext.request.contextPath}/store/deleteStore.action", {"id": data.id}, function (result) {
                        layer.msg(result.msg);
                        //刷新表格数据
                        tableIns.reload();
                    })
                })
            } else if (layEvent == 'edit') {
                StoreProcess(data);
            }
        })

        //审批仓库信息
        function StoreProcess(data) {
            mainIndex = layer.open({
                type: 1,
                title: '审批仓库信息',
                content: $("#saveOrUpdateDiv"),
                area: ['700px', '480px'],
                success: function (index) {
                    //获取下拉框参数
                    // var typeId = $('#typeName option:selected').val();
                    //显示数据在表单中
                    form.val("dataFrm", data);
                    //设置图片
                    $("#showBookImg").attr("src", "${pageContext.request.contextPath}/file/downloadShowFile.action?path=" + data.image);
                    //设置打开的时候请求的地址,当我们点击提交的时候,整个数据给那个action提交
                    url = "${pageContext.request.contextPath}/store/ProcessStore.action";
                    $("#id").attr("readonly", "readonly");
                }
            })
        }

        //查看大图的方法
        function showBookImage(data) {
            layer.open({
                type: 1,
                title: "[" + data.bookName + "]的书籍图片",
                content: $("#viewBookImageDiv"),
                area: ['750px', '500px'],
                success: function (index) {
                    //通过改变img 的src属性让图片展示
                    $("#view_bookImg").attr("src", "${pageContext.request.contextPath}/file/downloadShowFile.action?path=" + data.image);
                }
            })
        }



        //监听头部工具栏
        table.on('toolbar(storeTable)', function (obj) {
            //判断到底是add还是批量删除
            switch (obj.event) {
                case 'add' :
                    openAddStore();
                    break;
                case 'deleteBatch':
                    deleteBatch();
                    break;
            }
        })


        //批量删除车辆信息
        function deleteBatch() {
            //得到选中的行信息
            var checkStatus = table.checkStatus("storeTable");
            var data = checkStatus.data; //拿到行数据
            //定义要发送给后台的参数  ids=123123&231231
            var params = "";
            $.each(data, function (i, item) {
                if (i == 0) {
                    params += "ids=" + item.id;
                } else {
                    params += "&ids=" + item.id;
                }
            });
            layer.confirm("您确认要删除这些仓库信息吗?", function (index) {
                //发送异步请求删除
                $.post("${pageContext.request.contextPath}/store/deleteBatchStore.action", params, function (res) {
                    layer.msg(res.msg);
                    //刷新表格
                    tableIns.reload();
                })
            })
        }


        //打开添加的页面
        function openAddStore() {
            mainIndex = layer.open({
                type: 1,
                title: '添加图书',
                content: $("#saveOrUpdateDiv"),
                area: ['700px', '480px'],
                success: function (index) {
                    //清除表单的数据
                    $("#dataFrm")[0].reset();
                    //设置默认图片
                    $("#showBookImg").attr("src", "${pageContext.request.contextPath}/file/downloadShowFile.action?path=images/defaultBookImage.jpg");
                    //设置图片的value值
                    $("#image").val("images/defaultBookImage.jpg");
                    //设置打开的时候请求的地址,当我们点击提交的时候,整个数据给那个action提交
                    url = "${pageContext.request.contextPath}/book/addBook.action";
                    //删除id的readonly
                    $("#id").removeAttr("readonly", "readonly");
                }
            })
        }

        //获取下拉框typeId
        var typeId;
        form.on("select(type)",function (data) {
            typeId = data.value;
        })

        //保存数据,提交表单
        form.on("submit(doSubmit)", function (obj) {
            //获取下拉框参数
            // var typeId = $('#typeName option:selected').val();
            //序列化方式获取表单的数据
            var params = $("#dataFrm").serialize();
            // params += "&typeId=" + typeId
            // alert("params=" + params)
            $.post(url, params, function (obj) {
                layer.msg(obj.msg);
                //关闭弹出层
                layer.close(mainIndex);
                //刷新表格
                tableIns.reload();
            })
        })


        //上传图片
        upload.render({
            elem: "#bookImgDiv",
            url: "${pageContext.request.contextPath}/file/uploadFile.action",
            method: "post",
            acceptMime: "images/*",
            field: "mf",
            done: function (res, index, upload) {
                //设置图片的src属性,目的是让图片在弹出层中显示
                $("#showBookImg").attr("src", "${pageContext.request.contextPath}/file/downloadShowFile.action?path=" + res.data.src);
                //还要设置input的中的value值
                $("#image").val(res.data.src);
                //设置背景样式
                $("#bookImgDiv").css("background", "#fff");
            }
        })
    })

</script>
</body>
</html>


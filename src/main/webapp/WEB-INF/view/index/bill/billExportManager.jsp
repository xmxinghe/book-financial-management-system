<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>账单报表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="icon" href="favicon.ico">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/public.css" media="all"/>
</head>
<body class="childrenBody">

<!-- 搜索条件开始 -->
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查询条件</legend>
</fieldset>
<form class="layui-form" method="post" id="searchFrm">
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">起始时间:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="createTime" id="createTime" autocomplete="off"
                       class="layui-input layui-input-inline"
                       placeholder="请选择起始创建时间" style="height: 30px;border-radius: 10px">
            </div>
        </div>
<%--        <div class="layui-inline">--%>
<%--            <label class="layui-form-label">结束时间:</label>--%>
<%--            <div class="layui-input-inline" style="padding: 5px">--%>
<%--                <input type="text" name="createTime" id="endTime" autocomplete="off"--%>
<%--                       class="layui-input layui-input-inline"--%>
<%--                       placeholder="请选择结束时间" style="height: 30px;border-radius: 10px">--%>
<%--            </div>--%>
<%--        </div>--%>
        <div class="layui-inline">
            <button type="button"
                    class="layui-btn layui-btn-normal layui-icon layui-icon-search layui-btn-radius layui-btn-sm"
                    id="doSearch" style="margin-top: 4px">查询
            </button>
            <button type="reset"
                    class="layui-btn layui-btn-warm layui-icon layui-icon-refresh layui-btn-radius layui-btn-sm"
                    style="margin-top: 4px">重置
            </button>
        </div>
    </div>

</form>

<!-- 数据表格开始 -->
<table class="layui-hide" id="billTable" lay-filter="billTable"></table>
<div id="billToolBar" style="display: none;">
    <button type="button" class="layui-btn layui-btn-green layui-btn-xs layui-btn-radius" lay-event="exportInfo">
        导出月季表
    </button>
</div>
</div>
</form>
</div>


<script src="${pageContext.request.contextPath}/resources/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['jquery', 'layer', 'form', 'table', 'upload', 'laydate'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var upload = layui.upload;
        var laydate = layui.laydate;

        //渲染日期表格
        laydate.render({
            elem: '#createTime',
            type: 'datetime'
        })

        //渲染日期表格
        laydate.render({
            elem: '#endTime',
            type: 'datetime'
        })


        //渲染数据表格
        tableIns = table.render({
            elem: '#billTable', //渲染的表格对象
            url: '${pageContext.request.contextPath}/bill/loadAllBill.action', //请求数据接口的地址
            title: '图书流水数据表',
            toolbar: '#billToolBar',
            height: 'full-205',
            cellMinWidth: 100, //设置列的最小默认宽度
            page: true, //开启分页
            cols: [[
                {type: 'checkbox', fixed: 'left'},
                {field: 'id', title: '账单编号', align: 'center', width: '100'},
                {field: 'billName', title: '账单所属人', align: 'center', width: '120'},
                {field: 'title', title: '账单标题', align: 'center', width: '120'},
                {
                    field: 'status', title: '账单类型', align: 'center', width: '120', templet: function (d) {
                        return d.status == '1' ? '<font color=red>收入</font>' : '<font color=#adff2f>支出</font>'
                    }
                },
                {field: 'amount', title: '账单金额', align: 'center', width: '100'},
                {field: 'createTime', title: '创建时间', align: 'center', width: '160'}
            ]]
        })

        //模糊按条件查询
        $("#doSearch").click(function () {
                //获取表单的参数
                var creteTime = $("#createTime").val();
                // var endTime = $("#endTime").val();
                var params = "createTime=" + creteTime;
                // alert(params)
                tableIns.reload({
                    url: "${pageContext.request.contextPath}/bill/loadAllBillByTime.action?" + params,
                    page: {curr: 1}
                })
            }
        )

        var mainIndex;
        var url;


        //监听头部工具栏
        table.on('toolbar(billTable)', function (obj) {
            //判断到底是add还是批量删除
            switch (obj.event) {
                case 'exportInfo':
                    exportInfo(obj);
                    break
            }
        })


        //导出月报表
        function exportInfo(obj) {
            var creteTime = $("#createTime").val();
            // var endTime = $("#endTime").val();
            // alert(params);
            if (creteTime==null || creteTime==0 || creteTime=="") {
                layer.msg("请选择月份后再导出月报表");
            } else {
                window.location.href = "${pageContext.request.contextPath}/bill/exportMonthInfo.action?" + "createTime=" + creteTime;
            }
        }
    })
</script>
</body>
</html>


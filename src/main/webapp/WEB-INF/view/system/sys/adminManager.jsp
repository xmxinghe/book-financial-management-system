<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <meta charset="utf-8">
    <title>管理员管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <%--<link rel="icon" href="favicon.ico">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/public.css" media="all"/>
</head>
<body class="childrenBody">

<!-- 搜索条件开始 -->
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查询条件</legend>
</fieldset>
<form class="layui-form" method="post" id="searchFrm">

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">管理员名称:</label>
            <div class="layui-input-inline" style="padding: 5px">
                <input type="text" name="adminName" autocomplete="off" class="layui-input layui-input-inline"
                       placeholder="请输入管理员名称" style="height: 30px;border-radius: 10px">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">账号状态:</label>
            <div class="layui-input-inline">
                <input type="radio" name="status" value="0" title="启用">
                <input type="radio" name="status" value="1" title="禁用">
            </div>
            <button type="button"
                    class="layui-btn layui-btn-normal layui-icon layui-icon-search layui-btn-radius layui-btn-sm"
                    id="doSearch" style="margin-top: 4px">查询
            </button>
            <button type="reset"
                    class="layui-btn layui-btn-warm layui-icon layui-icon-refresh layui-btn-radius layui-btn-sm"
                    style="margin-top: 4px">重置
            </button>
        </div>
    </div>

</form>

<!-- 数据表格开始 -->
<table class="layui-hide" id="adminTable" lay-filter="adminTable"></table>
<div id="adminToolBar" style="display: none;">
    <button type="button" class="layui-btn layui-btn-sm layui-btn-radius" lay-event="add">增加</button>
    <button type="button" class="layui-btn layui-btn-danger layui-btn-sm layui-btn-radius" lay-event="deleteBatch">
        批量删除
    </button>
</div>
<div id="adminBar" style="display: none;">
    <a class="layui-btn layui-btn-xs layui-btn-radius" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs layui-btn-radius" lay-event="del">删除</a>
</div>

<!-- 添加和修改的弹出层-->
<div style="display: none;padding: 20px" id="saveOrUpdateDiv">
    <form class="layui-form layui-row layui-col-space10" lay-filter="dataFrm" id="dataFrm">
        <div class="layui-col-md12 layui-col-xs12">
            <div class="layui-row layui-col-space10">
                <div class="layui-col-md9 layui-col-xs7">
                    <div class="layui-form-item magt3" hidden>
                        <label class="layui-form-label">管理员编号:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="id" id="id" autocomplete="off" class="layui-input"
                                   placeholder="请输入管理员编号" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item magt3">
                        <label class="layui-form-label">管理员名称:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="text" name="adminName" id="adminName" autocomplete="off" class="layui-input"
                                   lay-verify="required"
                                   placeholder="请输入管理员名称" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                    <div class="layui-form-item magt3">
                        <label class="layui-form-label">管理员密码:</label>
                        <div class="layui-input-block" style="padding: 5px">
                            <input type="password" name="password" id="password" autocomplete="off" class="layui-input"
                                   lay-verify="required"
                                   placeholder="请输入管理员密码" style="height: 30px;border-radius: 10px">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item magb0">
                <div class="layui-inline">
                    <label class="layui-form-label">账号状态:</label>
                    <div class="layui-input-inline">
                        <input type="radio" name="status" value="0" checked="checked" title="启用">
                        <input type="radio" name="status" value="1" title="禁用">
                    </div>
                </div>
            </div>
            <div class="layui-form-item magb0">
                <div class="layui-input-block" style="text-align: center;padding-right: 120px">
                    <button type="button"
                            class="layui-btn layui-btn-normal layui-btn-md layui-icon layui-icon-release layui-btn-radius"
                            lay-filter="doSubmit" lay-submit="">提交
                    </button>
                    <button type="reset"
                            class="layui-btn layui-btn-warm layui-btn-md layui-icon layui-icon-refresh layui-btn-radius">
                        重置
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>


<script src="${pageContext.request.contextPath}/resources/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['jquery', 'layer', 'form', 'table', 'upload', 'laydate'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var upload = layui.upload;
        var laydate = layui.laydate;

        //渲染数据表格
        tableIns = table.render({
            elem: '#adminTable', //渲染的表格对象
            url: '${pageContext.request.contextPath}/admin/loadAllAdmin.action', //请求数据接口的地址
            title: '管理员信息表',
            toolbar: '#adminToolBar',
            height: 'full-205',
            cellMinWidth: 100, //设置列的最小默认宽度
            page: true, //开启分页
            cols: [[
                {type: 'checkbox', fixed: 'left'},
                {field: 'id', title: '管理员编号', align: 'center', width: '100'},
                {field: 'adminName', title: '用户名', align: 'center', width: '90'},
                {
                    field: 'status', title: '账号状态', align: 'center', width: '140', templet: function (d) {
                        return d.status == '0' ? '<font color=#adff2f>启用</font>' : '<font color=red>禁用</font>'
                    }
                },
                {field: 'createTime', title: '创建时间', align: 'center', width: '160'},
                {fixed: 'right', title: '操作', toolbar: '#adminBar', align: 'center', width: '200'}
            ]]
        })

        //模糊按条件查询
        $("#doSearch").click(function () {
                //获取表单的参数
                var params = $("#searchFrm").serialize();
                tableIns.reload({
                    url: "${pageContext.request.contextPath}/admin/loadAllAdmin.action?" + params,
                    page: {curr: 1}
                })
            }
        )

        var mainIndex;
        var url;


        //监听行工具栏
        table.on('tool(adminTable)', function (obj) {
            //获取当前的行数据
            var data = obj.data;
            //获取事件
            var layEvent = obj.event;
            if (layEvent == 'del') {
                layer.confirm("是否确认删除[" + data.adminName + "]这个管理员?", function (index) {
                    //发送ajax请求
                    $.get("${pageContext.request.contextPath}/admin/deleteAdmin.action", {"id": data.id}, function (result) {
                        layer.msg(result.msg);
                        //刷新表格数据
                        tableIns.reload();
                    })
                })
            } else if (layEvent == 'edit') {
                updateAdmin(data);
            }
        })

        function updateAdmin(data) {
            mainIndex = layer.open({
                type: 1,
                title: '修改管理员信息',
                content: $("#saveOrUpdateDiv"),
                area: ['700px', '480px'],
                success: function (index) {
                    //显示数据在表单中
                    form.val("dataFrm", data);
                    //设置打开的时候请求的地址,当我们点击提交的时候,整个数据给那个action提交
                    url = "${pageContext.request.contextPath}/admin/updateAdmin.action";
                    //删除readonly
                    $("#id").attr("readonly", "readonly");
                }
            })
        }


        //监听头部工具栏
        table.on('toolbar(adminTable)', function (obj) {
            //判断到底是add还是批量删除
            switch (obj.event) {
                case 'add' :
                    openAddAdmin();
                    break;
                case 'deleteBatch':
                    deleteBatch();
                    break;
            }
        })


        //批量删除车辆信息
        function deleteBatch() {
            //得到选中的行信息
            var checkStatus = table.checkStatus("adminTable");
            var data = checkStatus.data; //拿到行数据
            //定义要发送给后台的参数  ids=123123&231231
            var params = "";
            $.each(data, function (i, item) {
                if (i == 0) {
                    params += "ids=" + item.id;
                } else {
                    params += "&ids=" + item.id;
                }
            });
            layer.confirm("您确认要删除这些管理员吗?", function (index) {
                //发送异步请求删除
                $.post("${pageContext.request.contextPath}/admin/deleteBatchAdmin.action", params, function (res) {
                    layer.msg(res.msg);
                    //刷新表格
                    tableIns.reload();
                })
            })
        }


        //打开添加的页面
        function openAddAdmin() {
            mainIndex = layer.open({
                type: 1,
                title: '添加管理员',
                content: $("#saveOrUpdateDiv"),
                area: ['700px', '480px'],
                success: function (index) {
                    //清除表单的数据
                    $("#dataFrm")[0].reset();
                    //设置打开的时候请求的地址,当我们点击提交的时候,整个数据给那个action提交
                    url = "${pageContext.request.contextPath}/admin/addAdmin.action";
                    //删除id的readonly
                    $("#id").removeAttr("readonly", "readonly");
                }
            })
        }

        //保存数据,提交表单
        form.on("submit(doSubmit)", function (obj) {
            //获取下拉框参数
            // var typeId = $('#typeName option:selected').val();
            //序列化方式获取表单的数据
            var params = $("#dataFrm").serialize();
            // params += "&typeId=" + typeId
            // alert("params=" + params)
            $.post(url, params, function (obj) {
                layer.msg(obj.msg);
                //关闭弹出层
                layer.close(mainIndex);
                //刷新表格
                tableIns.reload();
            })
        })
    })

</script>
</body>
</html>

